using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TwinCAT.Ads;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace csBeifuConnect
{
    
    class PLCConnect
    {
        private int m_iTrigger;
        private int m_iCCDFinished;
        private int m_iResult;
        private int m_iNum;

        private bool m_bTrigger;
        private bool m_bCCDFinished;
        private bool m_bResult;
        private byte m_byteNum;

        private TcAdsClient adsClient;        

        public bool BoolTrigger
        {
            get { return m_bTrigger; }
            set { m_bTrigger = value; }
        }
        public bool BoolCCDFinished
        {
            get { return m_bCCDFinished; }
            set { m_bCCDFinished = value;}
        }
        public bool BoolResult
        {
            get { return m_bResult; }
            set { m_bResult = value; }
        }
        public byte ByteClamp
        {
            get { return m_byteNum; }
            set { m_byteNum = value; }
        }
        public void InitConnect()
        {
            adsClient = new TcAdsClient();            
            try
            {
                //Global_Variables adsClient.AdsNotificationEx += new AdsNotificationExEventHandler(adsClient_AdsNotificationEx);
                string str = "172.168.250.32.1.1";
                adsClient.Connect(str, 801);
                m_iTrigger = adsClient.CreateVariableHandle(".m_bTrigger");
                m_iCCDFinished = adsClient.CreateVariableHandle(".m_bCCDFinished");
                m_iResult = adsClient.CreateVariableHandle(".m_bResult");
                m_iNum = adsClient.CreateVariableHandle(".m_byteNum");               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }  
        
        public void ReadMessage()
        {
            try
            {              

                m_bTrigger = (bool)adsClient.ReadAny(m_iTrigger, typeof(bool));
                m_bCCDFinished = (bool)adsClient.ReadAny(m_iCCDFinished, typeof(bool));
                m_bResult = (bool)adsClient.ReadAny(m_iResult, typeof(bool));
                m_byteNum = (byte)adsClient.ReadAny(m_iNum, typeof(byte));                
            }
            catch (Exception ex)
            {
                
            }
        }
        public void WriteMessage()
        {

            try
            {
                //adsClient.WriteAny(m_iTrigger,m_bTrigger);
                adsClient.WriteAny(m_iCCDFinished, m_bCCDFinished);
                adsClient.WriteAny(m_iResult, m_bResult);
                adsClient.WriteAny(m_iNum, m_byteNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }      
       
    }
}
