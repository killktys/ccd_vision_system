using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIS.Imaging;
using TIS.Imaging.VCDHelpers;
using HalconDotNet;
using System.Threading;
namespace csImagingCamera
{
    class Camera
    {
        protected TIS.Imaging.ICImagingControl icImagingControl = new TIS.Imaging.ICImagingControl();
        protected HObject img = new HObject();
        protected HWindow WinID;
        protected TIS.Imaging.VCDSwitchProperty PolaritySwitch;
        protected TIS.Imaging.VCDButtonProperty st;
        protected bool bGrab = false;
        protected ManualResetEvent m_AnalyEvent = new ManualResetEvent(false);

        public ManualResetEvent AnalyEvent
        {
            get { return m_AnalyEvent; }
            set { m_AnalyEvent = value; }
        }
        public HObject Image
        {
            get { return img; }
            set { img = value; }
        }
        public HWindow hWindow
        {
            get { return WinID; }
            set { WinID = value; }
        }
        public ICImagingControl IC
        {
            get { return icImagingControl; }
            set { icImagingControl = value; }
        }
        public bool InitCamera(string str)
        {
            try
            {
                icImagingControl.LoadDeviceStateFromFile(str, true);//"device1.xml"
            }
            catch (System.Exception ex)
            {
                icImagingControl.ShowDeviceSettingsDialog();
                icImagingControl.SaveDeviceStateToFile(str);
            }
            try
            {
                icImagingControl.ImageAvailable += new System.EventHandler<TIS.Imaging.ICImagingControl.ImageAvailableEventArgs>(TIS_ImageAvailable);//注册回调事件
                icImagingControl.LiveDisplayDefault = false;
                icImagingControl.LiveCaptureContinuous = true;
                //icImagingControl1.MemoryCurrentGrabberColorformat = TIS.Imaging.ICImagingControlColorformats.ICRGB32 ;
                icImagingControl.MemoryCurrentGrabberColorformat = TIS.Imaging.ICImagingControlColorformats.ICY800;
                SetTrigger();
                icImagingControl.LiveStart();
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void StopCamera()
        {
            icImagingControl.LiveStop();
        }
        public void StartCamera()
        {
            icImagingControl.LiveStart();
        }
        public void DestroyCamera()
        {
            icImagingControl.Dispose();
        }
        public void ContinueCamera()
        {
            PolaritySwitch.Switch = false;
            icImagingControl.LiveStart();
        }        
        public void SnapImage()
        {
            bGrab = false;
            if (PolaritySwitch.Switch)
            {
                st.Push();
            }
            else
            {
                PolaritySwitch.Switch = true;
                st.Push();
            }
                   
           
        }
        public void Property(string str)
        {
            icImagingControl.ShowPropertyDialog();
            icImagingControl.SaveDeviceStateToFile(str);//device.xml
        }
        private object obj = new object();
        private void TIS_ImageAvailable(object sender, ICImagingControl.ImageAvailableEventArgs e)
        {
            try
            {
                lock (obj)
                {
                    TIS.Imaging.ICImagingControl IC_Callback = sender as TIS.Imaging.ICImagingControl;
                    //Thread.Sleep(1);
                    HTuple type, width, height, point;

                    //HObject img;
                    IntPtr ImageData = e.ImageBuffer.GetImageDataPtr();


                    img.Dispose();
                    img = null;
                    //HOperatorSet.GenImageInterleaved(out img, ImageData, "bgrx", e.ImageBuffer.Size.Width, e.ImageBuffer.Size.Height, -1, "byte", e.ImageBuffer.Size.Width, e.ImageBuffer.Size.Height, 0, 0, -1, 0);
                    HOperatorSet.GenImage1(out img, "byte", e.ImageBuffer.Size.Width, e.ImageBuffer.Size.Height, ImageData);
                    HOperatorSet.GetImagePointer1(img, out point, out type, out width, out height);
                    //HOperatorSet.CopyImage(img, out imgTemp);  

                    if (img != null)
                    {
                        m_AnalyEvent.Set();
                        WinID.SetPart(0, 0, height, width);
                        HOperatorSet.DispObj(img, WinID);
                    }
                }
            }
            catch
            {
                this.SnapImage();
            }
            
        }
     
        private void SetTrigger()
        {
            PolaritySwitch = (TIS.Imaging.VCDSwitchProperty)icImagingControl.VCDPropertyItems.FindInterface(TIS.Imaging.VCDIDs.VCDID_TriggerMode + ":" + TIS.Imaging.VCDIDs.VCDElement_Value + ":" + TIS.Imaging.VCDIDs.VCDInterface_Switch);
            st = (TIS.Imaging.VCDButtonProperty)icImagingControl.VCDPropertyItems.FindInterface(TIS.Imaging.VCDIDs.VCDID_TriggerMode + ":{FDB4003C-552C-4FAA-B87B-42E888D54147}:" + TIS.Imaging.VCDIDs.VCDInterface_Button);
            PolaritySwitch.Switch = true;
            
        }
        public void ReConnectCamera(string str)
        {
            icImagingControl.LiveStop();
            icImagingControl.ShowDeviceSettingsDialog();
            icImagingControl.SaveDeviceStateToFile(str);
        }
    }
}
