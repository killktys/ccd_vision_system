using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace csPlcConnect
{
    public class PLCConnect
    {
        private TcpClient tcpClient;//与服务器的连接
        private string ip = ""; //PLC 的ip地址
        private bool plcconnectstate;
        public string Ip
        {
            get { return ip; }
            set { ip = value; }
        }
        private string port = "9600";//PLC的端口号，默认是9600
        public string Port
        {
            get { return port; }
            set { port = value; }
        }
        private NetworkStream stream;//与服务器数据交互的通道
        /// <summary>
        /// 建立PLC数据流
        /// </summary>
        /// <returns></returns>
        /// 
       
          
       
        public bool DataStreamFound()
        {
            if (string.IsNullOrEmpty(ip))
            {
                MessageBox.Show("ip地址不能为空！！！");
                return false ;
            }
            try
            {
                if (stream != null)
                {
                    stream.Close();
                }
                if (tcpClient != null)
                {
                    tcpClient.Close();
                }
                tcpClient = new TcpClient();
                tcpClient.Connect(IPAddress.Parse(ip), Int32.Parse(port)); //向指定的服务器发出连接请求
                stream = tcpClient.GetStream(); //获得与服务器数据交互的流通道
                tcpClient.SendTimeout = 5000;
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 关闭PLC连接
        /// </summary>
        public void DataStreamClose()
        {
            if (stream != null)
            {
                stream.Close();
            }
            if (tcpClient != null)
            {
                tcpClient.Close();
            }
            stream = null;
            tcpClient = null;
        }

        private string SendMath(Byte[] outbytes)
        {
            string msg = "";
            try
            {
                stream.Write(outbytes, 0, outbytes.Length);
                msg = ServerResponse();
            }
            catch
            {
                return null;
            }
            return msg;
        }

        private string ServerResponse()
        {
            string msg = "";
            byte[] buff = new byte[1024];
            int len;
            if (!stream.CanRead)
            {
                return null;
            }
            len = stream.Read(buff, 0, buff.Length);
            if (len < 1)
            {
                return null;
            }
            string data = "";
            for (int i = 0; i < len; i++)
            {
                data += "%" + Convert.ToString(buff[i], 16);
            }
            msg = HexToDec(data);
            return msg;
        }

        private string HexToDec(string ms)
        {
            int i;
            i = ms.LastIndexOf("%");
            string s = ms.Substring(i + 1, ms.Length - i - 1);
            if (s.Length < 2)
            {
                s = "0" + s;
            }
            string s1 = ms.Substring(0, i);
            int x;
            x = s1.LastIndexOf("%");
            string s2 = s1.Substring(x + 1, s1.Length - x - 1);
            if (s2.Length < 2)
            {
                s2 = "0" + s2;
            }
            s = s2 + s;
            int m = Convert.ToInt32(s, 16);
            return m.ToString();
        }
        /// <summary>
        /// 数据流建立完成，与PLC握手，建立连接
        /// </summary>
        /// <param name="localip">本机IP地址最后节点</param>
        /// <param name="plcip">PLC 的IP地址最后节点</param>
        /// <returns>连接是否成功</returns>
        public bool ShakeHands(int localip, int plcip)
        {
            Byte[] outbytes = { 0x46, 0x49, 0x4E, 0x53, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xD3 };
            //                    46    49、4E      53、00      00、00      0C、00      00、   00   00、  00    00、  00    00、  00    00、  00    C7
            //  注释：
            //  1.	长度固定 20个字
            //  2.	最后一个字节表示本地计算机IP地址节点号（本地IP最后位199=C7）
            outbytes[18] = Convert.ToByte(Convert.ToInt32(localip) / 256);
            outbytes[19] = Convert.ToByte(Convert.ToInt32(localip) % 256);
            string msg;
            msg = SendMath(outbytes);
            int _iMsg = int.Parse(msg);
            if (_iMsg == plcip)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 写单个地址
        /// </summary>
        /// <param name="localip">本机IP地址最后节点</param>
        /// <param name="plcip">PLC 的IP地址最后节点</param>
        /// <param name="addr">地址</param>
        /// <param name="array">写值</param>
        /// <returns></returns>
        public string WriteToPlc(int localip, int plcip, int addr, int array)
        {
            string msg = "";
            Byte[] outbytes = { 0x46, 0x49, 0x4E, 0x53, 0x00, 0x00, 0x00, 0x1C, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x02, 0x00, 0x08, 0x00, 0x00, 0xCD, 0x00, 0xFF, 0x01, 0x02, 0x82, 0x01, 0x2F, 0x00, 0x00, 0x01, 0x00, 0x00 };
            outbytes[19] = Convert.ToByte(plcip / 256);
            outbytes[20] = Convert.ToByte(plcip % 256);
            //指定本地IP
            outbytes[22] = Convert.ToByte(localip / 256);
            outbytes[23] = Convert.ToByte(localip % 256);
            outbytes[29] = Convert.ToByte(addr / 256);
            outbytes[30] = Convert.ToByte(addr % 256);
            outbytes[34] = Convert.ToByte(array / 256);
            outbytes[35] = Convert.ToByte(array % 256);
            msg = SendMath(outbytes);
            return msg;
        }
        /// <summary>
        /// 写多个地址
        /// </summary>
        /// <param name="localip">本机IP地址最后节点</param>
        /// <param name="plcip">PLC的IP地址最后节点</param>
        /// <param name="addr">起始地址</param>
        /// <param name="array">值</param>
        /// <returns></returns>
        public string WriteToPlcs(int localip, int plcip, int addr, int[] array)
        {
            int length = array.Length;
            string msg = "";
            Byte[] outb = { 0x46, 0x49, 0x4E, 0x53, 0x00, 0x00, 0x00, 0x1C, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x02, 0x00, 0x08, 0x00, 0x00, 0xCD, 0x00, 0xFF, 0x01, 0x02, 0x82, 0x01, 0x2F, 0x00, 0x00, 0x01, 0x00, 0x00 };
            Byte[] outbytes = new Byte[outb.Length + (length - 1) * 2];
            for (int i = 0; i < outbytes.Length; i++)
            {
                if (i < outb.Length - 2)
                {
                    outbytes[i] = outb[i];
                }
                else
                {
                    outbytes[i] = Convert.ToByte(0 % 256);
                }
            }
            outbytes[32] = Convert.ToByte(length / 256);
            outbytes[33] = Convert.ToByte(length % 256);
            outbytes[29] = Convert.ToByte(addr / 256);
            outbytes[30] = Convert.ToByte(addr % 256);
            length = 26 + 2 * length;
            outbytes[6] = Convert.ToByte(length / 256);
            outbytes[7] = Convert.ToByte(length % 256);
            outbytes[19] = Convert.ToByte(plcip / 256);
            outbytes[20] = Convert.ToByte(plcip % 256);
            outbytes[22] = Convert.ToByte(localip / 256);
            outbytes[23] = Convert.ToByte(localip % 256);
            for (int i = 0; i < array.Length; i++)
            {
                outbytes[outb.Length - 2 + i * 2] = Convert.ToByte(array[i] / 256);
                outbytes[outb.Length - 1 + i * 2] = Convert.ToByte(array[i] % 256);
            }
            msg = SendMath(outbytes);
            return msg;
        }
        /// <summary>
        /// 读单个地址
        /// </summary>
        /// <param name="localip">本机IP地址最后节</param>
        /// <param name="plcip">PLC的IP地址最后节点</param>
        /// <param name="addr">地址</param>
        /// <returns>值</returns>
        public string ReadFormPlc(int localip, int plcip, int addr)
        {
            string msg = "";
            Byte[] outbyte = { 0x46, 0x49, 0x4E, 0x53, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x02, 0x00, 0x09, 0x00, 0x00, 0xD3, 0x00, 0xFF, 0x01, 0x01, 0x82, 0x01, 0x02, 0x00, 0x00, 0x01 };
            //制定PLC的IP地址
            outbyte[19] = Convert.ToByte(plcip / 256);
            outbyte[20] = Convert.ToByte(plcip % 256);
            //指定本地IP
            outbyte[22] = Convert.ToByte(localip / 256);
            outbyte[23] = Convert.ToByte(localip % 256);
            //设备地址
            outbyte[29] = Convert.ToByte(addr / 256);
            outbyte[30] = Convert.ToByte(addr % 256);
            msg = SendMath(outbyte);
            return msg;
        }
        /// <summary>
        /// 读多个地址
        /// </summary>
        /// <param name="localip">本机IP地址最后节</param>
        /// <param name="plcip">PLC的IP地址最后节点</param>
        /// <param name="addr">地址</param>
        /// <param name="length">长度</param>
        /// <returns>值</returns>
        public string[] ReadFromPlcs(int localip, int plcip, int addr, int length)
        {
            string[] msg = new string[length];
            Byte[] outbytes = { 0x46, 0x49, 0x4E, 0x53, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x02, 0x00, 0x08, 0x00, 0x00, 0xCD, 0x00, 0xFF, 0x01, 0x01, 0x82, 0x00, 0x00, 0x00, 0x00, 0x01 };
            outbytes[19] = Convert.ToByte(plcip / 256);
            outbytes[20] = Convert.ToByte(plcip % 256);
            outbytes[22] = Convert.ToByte(localip / 256);
            outbytes[23] = Convert.ToByte(localip % 256);
            outbytes[29] = Convert.ToByte(addr / 256);
            outbytes[30] = Convert.ToByte(addr % 256);
            outbytes[32] = Convert.ToByte(length / 256);
            outbytes[33] = Convert.ToByte(length % 256);
            try
            {
                stream.Write(outbytes, 0, outbytes.Length);
            }
            catch
            {
                plcconnectstate = false;
            }
            msg = ServerResponses(length);
            return msg;
        }
        private string[] ServerResponses(int length)
        {
            string[] msg = new string[length];
            byte[] buff = new byte[1024];
            int len;
            if (!stream.CanRead)
            {
                return null;
            }
            try
            {
                len = stream.Read(buff, 0, buff.Length);
            }
            catch
            {
                len = 0;
            }
            if (len < 1)
            {
                return null;
            }
            string data = "";
            for (int i = 0; i < len; i++)
            {
                data += "%" + Convert.ToString(buff[i], 16);
            }
            for (int i = length; i > 0; i--)
            {
                string[] ay = HexToDecs(data);
                msg[i - 1] = ay[0];
                data = ay[1];
            }
            return msg;
        }
        private string[] HexToDecs(string ms)
        {
            string[] strs = new string[2];
            int i;
            i = ms.LastIndexOf("%");
            string s = ms.Substring(i + 1, ms.Length - i - 1);
            if (s.Length < 2)
            {
                s = "0" + s;
            }
            string s1 = ms.Substring(0, i);
            int x;
            x = s1.LastIndexOf("%");
            string s2 = s1.Substring(x + 1, s1.Length - x - 1);
            if (s2.Length < 2)
            {
                s2 = "0" + s2;
            }
            s = s2 + s;
            int n = Convert.ToInt32(s, 16);
            strs[0] = n.ToString();
            strs[1] = s1.Substring(0, x);
            return strs;
        }
    }
}
