﻿namespace C17Software
{
    partial class ResultAnalyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCameraName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDistance1 = new System.Windows.Forms.TextBox();
            this.tbDistance2 = new System.Windows.Forms.TextBox();
            this.tbDistance3 = new System.Windows.Forms.TextBox();
            this.tbDistance4 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbDistance11 = new System.Windows.Forms.TextBox();
            this.tbDistance21 = new System.Windows.Forms.TextBox();
            this.tbDistance31 = new System.Windows.Forms.TextBox();
            this.tbDistance41 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbCameraName
            // 
            this.cbCameraName.FormattingEnabled = true;
            this.cbCameraName.Items.AddRange(new object[] {
            "相机1",
            "相机2",
            "相机3",
            "相机4"});
            this.cbCameraName.Location = new System.Drawing.Point(114, 12);
            this.cbCameraName.Name = "cbCameraName";
            this.cbCameraName.Size = new System.Drawing.Size(190, 20);
            this.cbCameraName.TabIndex = 0;
            this.cbCameraName.Text = "相机1";
            this.cbCameraName.SelectedIndexChanged += new System.EventHandler(this.cbCameraName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "相机名称：";
            // 
            // tbDistance1
            // 
            this.tbDistance1.Location = new System.Drawing.Point(114, 61);
            this.tbDistance1.Name = "tbDistance1";
            this.tbDistance1.Size = new System.Drawing.Size(72, 21);
            this.tbDistance1.TabIndex = 2;
            // 
            // tbDistance2
            // 
            this.tbDistance2.Location = new System.Drawing.Point(114, 118);
            this.tbDistance2.Name = "tbDistance2";
            this.tbDistance2.Size = new System.Drawing.Size(72, 21);
            this.tbDistance2.TabIndex = 3;
            // 
            // tbDistance3
            // 
            this.tbDistance3.Location = new System.Drawing.Point(114, 179);
            this.tbDistance3.Name = "tbDistance3";
            this.tbDistance3.Size = new System.Drawing.Size(72, 21);
            this.tbDistance3.TabIndex = 4;
            // 
            // tbDistance4
            // 
            this.tbDistance4.Location = new System.Drawing.Point(114, 235);
            this.tbDistance4.Name = "tbDistance4";
            this.tbDistance4.Size = new System.Drawing.Size(72, 21);
            this.tbDistance4.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "1号位置：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "2号位置：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "3号位置：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "4号位置：";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(37, 293);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(78, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(226, 293);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tbDistance11
            // 
            this.tbDistance11.Location = new System.Drawing.Point(232, 61);
            this.tbDistance11.Name = "tbDistance11";
            this.tbDistance11.Size = new System.Drawing.Size(72, 21);
            this.tbDistance11.TabIndex = 12;
            // 
            // tbDistance21
            // 
            this.tbDistance21.Location = new System.Drawing.Point(232, 118);
            this.tbDistance21.Name = "tbDistance21";
            this.tbDistance21.Size = new System.Drawing.Size(72, 21);
            this.tbDistance21.TabIndex = 13;
            // 
            // tbDistance31
            // 
            this.tbDistance31.Location = new System.Drawing.Point(232, 179);
            this.tbDistance31.Name = "tbDistance31";
            this.tbDistance31.Size = new System.Drawing.Size(72, 21);
            this.tbDistance31.TabIndex = 14;
            // 
            // tbDistance41
            // 
            this.tbDistance41.Location = new System.Drawing.Point(232, 235);
            this.tbDistance41.Name = "tbDistance41";
            this.tbDistance41.Size = new System.Drawing.Size(72, 21);
            this.tbDistance41.TabIndex = 15;
            // 
            // ResultAnalyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 352);
            this.Controls.Add(this.tbDistance41);
            this.Controls.Add(this.tbDistance31);
            this.Controls.Add(this.tbDistance21);
            this.Controls.Add(this.tbDistance11);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDistance4);
            this.Controls.Add(this.tbDistance3);
            this.Controls.Add(this.tbDistance2);
            this.Controls.Add(this.tbDistance1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCameraName);
            this.Name = "ResultAnalyForm";
            this.Text = "ResultAnaly";
            this.Load += new System.EventHandler(this.ResultAnalyForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCameraName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDistance1;
        private System.Windows.Forms.TextBox tbDistance2;
        private System.Windows.Forms.TextBox tbDistance3;
        private System.Windows.Forms.TextBox tbDistance4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox tbDistance11;
        private System.Windows.Forms.TextBox tbDistance21;
        private System.Windows.Forms.TextBox tbDistance31;
        private System.Windows.Forms.TextBox tbDistance41;
    }
}