﻿namespace C17Software
{
    partial class SavePara
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbOK = new System.Windows.Forms.RadioButton();
            this.rbNG = new System.Windows.Forms.RadioButton();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rbOK
            // 
            this.rbOK.AutoSize = true;
            this.rbOK.Location = new System.Drawing.Point(37, 56);
            this.rbOK.Name = "rbOK";
            this.rbOK.Size = new System.Drawing.Size(83, 16);
            this.rbOK.TabIndex = 3;
            this.rbOK.TabStop = true;
            this.rbOK.Text = "仅存OK图像";
            this.rbOK.UseVisualStyleBackColor = true;
            // 
            // rbNG
            // 
            this.rbNG.AutoSize = true;
            this.rbNG.Location = new System.Drawing.Point(37, 23);
            this.rbNG.Name = "rbNG";
            this.rbNG.Size = new System.Drawing.Size(83, 16);
            this.rbNG.TabIndex = 4;
            this.rbNG.TabStop = true;
            this.rbNG.Text = "仅存NG图像";
            this.rbNG.UseVisualStyleBackColor = true;
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.Location = new System.Drawing.Point(37, 85);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(107, 16);
            this.rbAll.TabIndex = 5;
            this.rbAll.TabStop = true;
            this.rbAll.Text = "存储OK和NG图像";
            this.rbAll.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(21, 120);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(62, 34);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(107, 120);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(62, 34);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // SavePara
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(199, 169);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.rbAll);
            this.Controls.Add(this.rbNG);
            this.Controls.Add(this.rbOK);
            this.Name = "SavePara";
            this.Text = "存储设置窗口";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbOK;
        private System.Windows.Forms.RadioButton rbNG;
        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;

    }
}