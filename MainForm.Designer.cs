﻿namespace C17Software
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.hWindowControl1 = new HalconDotNet.HWindowControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCreateProject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOpenProject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveProject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOpenImg = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiImgShowCamera1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiImgShowCamera2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveImg = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiImgCamera1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiImgCamera2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.相机ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCamera1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReConnect1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProperty1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSnapImg1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiContinues1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCamera2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReConnect2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProperty2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSnapImg2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiContinues2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCamera3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReConnect3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProperty3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSnapImg3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiContinues3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCamera4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReConnect4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProperty4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSnapImg4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiContinues4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiImgAnaly = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrawCircle = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrawRec1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrawRec2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrawRec3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrawRec4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindCircle = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindCircle1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindCircle2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindCircle3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFindCircle4 = new System.Windows.Forms.ToolStripMenuItem();
            this.设置参数ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiParaImgAnaly = new System.Windows.Forms.ToolStripMenuItem();
            this.窗口1ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.窗口2ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiParaResult = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiParaSave = new System.Windows.Forms.ToolStripMenuItem();
            this.网络连接ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hWindowControl2 = new HalconDotNet.HWindowControl();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnData = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel8 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel12 = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.hWindowControl3 = new HalconDotNet.HWindowControl();
            this.hWindowControl4 = new HalconDotNet.HWindowControl();
            this.btnTrigger = new System.Windows.Forms.Button();
            this.btnResult = new System.Windows.Forms.Button();
            this.btnCCDFinish = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnectState = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbPLCState = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbOKRate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTotalPec = new System.Windows.Forms.TextBox();
            this.tbNGPec = new System.Windows.Forms.TextBox();
            this.tbOKPec = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // hWindowControl1
            // 
            this.hWindowControl1.BackColor = System.Drawing.Color.Black;
            this.hWindowControl1.BorderColor = System.Drawing.Color.Black;
            this.hWindowControl1.ImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindowControl1.Location = new System.Drawing.Point(20, 9);
            this.hWindowControl1.Name = "hWindowControl1";
            this.hWindowControl1.Size = new System.Drawing.Size(507, 415);
            this.hWindowControl1.TabIndex = 2;
            this.hWindowControl1.WindowSize = new System.Drawing.Size(507, 415);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.相机ToolStripMenuItem,
            this.tsmiImgAnaly,
            this.网络连接ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1257, 25);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCreateProject,
            this.tsmiOpenProject,
            this.tsmiSaveProject,
            this.tsmiOpenImg,
            this.tsmiSaveImg,
            this.tsmiExit});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // tsmiCreateProject
            // 
            this.tsmiCreateProject.Name = "tsmiCreateProject";
            this.tsmiCreateProject.Size = new System.Drawing.Size(124, 22);
            this.tsmiCreateProject.Text = "新建工程";
            this.tsmiCreateProject.Click += new System.EventHandler(this.tsmiCreateProject_Click);
            // 
            // tsmiOpenProject
            // 
            this.tsmiOpenProject.Name = "tsmiOpenProject";
            this.tsmiOpenProject.Size = new System.Drawing.Size(124, 22);
            this.tsmiOpenProject.Text = "打开工程";
            this.tsmiOpenProject.Click += new System.EventHandler(this.tsmiOpenProject_Click);
            // 
            // tsmiSaveProject
            // 
            this.tsmiSaveProject.Name = "tsmiSaveProject";
            this.tsmiSaveProject.Size = new System.Drawing.Size(124, 22);
            this.tsmiSaveProject.Text = "保存工程";
            this.tsmiSaveProject.Click += new System.EventHandler(this.tsmiSaveProject_Click);
            // 
            // tsmiOpenImg
            // 
            this.tsmiOpenImg.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiImgShowCamera1,
            this.tsmiImgShowCamera2});
            this.tsmiOpenImg.Name = "tsmiOpenImg";
            this.tsmiOpenImg.Size = new System.Drawing.Size(124, 22);
            this.tsmiOpenImg.Text = "打开图像";
            // 
            // tsmiImgShowCamera1
            // 
            this.tsmiImgShowCamera1.Name = "tsmiImgShowCamera1";
            this.tsmiImgShowCamera1.Size = new System.Drawing.Size(143, 22);
            this.tsmiImgShowCamera1.Text = "显示在窗口1";
            this.tsmiImgShowCamera1.Click += new System.EventHandler(this.tsmiImgShowCamera1_Click);
            // 
            // tsmiImgShowCamera2
            // 
            this.tsmiImgShowCamera2.Name = "tsmiImgShowCamera2";
            this.tsmiImgShowCamera2.Size = new System.Drawing.Size(143, 22);
            this.tsmiImgShowCamera2.Text = "显示在窗口2";
            this.tsmiImgShowCamera2.Click += new System.EventHandler(this.tsmiImgShowCamera2_Click);
            // 
            // tsmiSaveImg
            // 
            this.tsmiSaveImg.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiImgCamera1,
            this.tsmiImgCamera2});
            this.tsmiSaveImg.Name = "tsmiSaveImg";
            this.tsmiSaveImg.Size = new System.Drawing.Size(124, 22);
            this.tsmiSaveImg.Text = "保存图像";
            // 
            // tsmiImgCamera1
            // 
            this.tsmiImgCamera1.Name = "tsmiImgCamera1";
            this.tsmiImgCamera1.Size = new System.Drawing.Size(107, 22);
            this.tsmiImgCamera1.Text = "相机1";
            this.tsmiImgCamera1.Click += new System.EventHandler(this.tsmiImgCamera1_Click);
            // 
            // tsmiImgCamera2
            // 
            this.tsmiImgCamera2.Name = "tsmiImgCamera2";
            this.tsmiImgCamera2.Size = new System.Drawing.Size(107, 22);
            this.tsmiImgCamera2.Text = "相机2";
            this.tsmiImgCamera2.Click += new System.EventHandler(this.tsmiImgCamera2_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(124, 22);
            this.tsmiExit.Text = "退出程序";
            // 
            // 相机ToolStripMenuItem
            // 
            this.相机ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCamera1,
            this.tsmiCamera2,
            this.tsmiCamera3,
            this.tsmiCamera4});
            this.相机ToolStripMenuItem.Name = "相机ToolStripMenuItem";
            this.相机ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.相机ToolStripMenuItem.Text = "相机";
            // 
            // tsmiCamera1
            // 
            this.tsmiCamera1.Checked = true;
            this.tsmiCamera1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiCamera1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiReConnect1,
            this.tsmiProperty1,
            this.tsmiSnapImg1,
            this.tsmiContinues1});
            this.tsmiCamera1.Name = "tsmiCamera1";
            this.tsmiCamera1.Size = new System.Drawing.Size(107, 22);
            this.tsmiCamera1.Text = "相机1";
            // 
            // tsmiReConnect1
            // 
            this.tsmiReConnect1.Name = "tsmiReConnect1";
            this.tsmiReConnect1.Size = new System.Drawing.Size(124, 22);
            this.tsmiReConnect1.Text = "重连相机";
            this.tsmiReConnect1.Click += new System.EventHandler(this.tsmiReConnect1_Click);
            // 
            // tsmiProperty1
            // 
            this.tsmiProperty1.Name = "tsmiProperty1";
            this.tsmiProperty1.Size = new System.Drawing.Size(124, 22);
            this.tsmiProperty1.Text = "相机参数";
            this.tsmiProperty1.Click += new System.EventHandler(this.tsmiProperty1_Click);
            // 
            // tsmiSnapImg1
            // 
            this.tsmiSnapImg1.Name = "tsmiSnapImg1";
            this.tsmiSnapImg1.Size = new System.Drawing.Size(124, 22);
            this.tsmiSnapImg1.Text = "单帧采集";
            this.tsmiSnapImg1.Click += new System.EventHandler(this.tsmiSnapImg1_Click);
            // 
            // tsmiContinues1
            // 
            this.tsmiContinues1.Name = "tsmiContinues1";
            this.tsmiContinues1.Size = new System.Drawing.Size(124, 22);
            this.tsmiContinues1.Text = "连续采集";
            this.tsmiContinues1.Click += new System.EventHandler(this.tsmiContinues1_Click);
            // 
            // tsmiCamera2
            // 
            this.tsmiCamera2.Checked = true;
            this.tsmiCamera2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiCamera2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiReConnect2,
            this.tsmiProperty2,
            this.tsmiSnapImg2,
            this.tsmiContinues2});
            this.tsmiCamera2.Name = "tsmiCamera2";
            this.tsmiCamera2.Size = new System.Drawing.Size(107, 22);
            this.tsmiCamera2.Text = "相机2";
            // 
            // tsmiReConnect2
            // 
            this.tsmiReConnect2.Name = "tsmiReConnect2";
            this.tsmiReConnect2.Size = new System.Drawing.Size(124, 22);
            this.tsmiReConnect2.Text = "重连相机";
            this.tsmiReConnect2.Click += new System.EventHandler(this.tsmiReConnect2_Click);
            // 
            // tsmiProperty2
            // 
            this.tsmiProperty2.Name = "tsmiProperty2";
            this.tsmiProperty2.Size = new System.Drawing.Size(124, 22);
            this.tsmiProperty2.Text = "相机参数";
            this.tsmiProperty2.Click += new System.EventHandler(this.tsmiProperty2_Click);
            // 
            // tsmiSnapImg2
            // 
            this.tsmiSnapImg2.Name = "tsmiSnapImg2";
            this.tsmiSnapImg2.Size = new System.Drawing.Size(124, 22);
            this.tsmiSnapImg2.Text = "单帧采集";
            this.tsmiSnapImg2.Click += new System.EventHandler(this.tsmiSnapImg2_Click);
            // 
            // tsmiContinues2
            // 
            this.tsmiContinues2.Name = "tsmiContinues2";
            this.tsmiContinues2.Size = new System.Drawing.Size(124, 22);
            this.tsmiContinues2.Text = "连续采集";
            this.tsmiContinues2.Click += new System.EventHandler(this.tsmiContinues2_Click);
            // 
            // tsmiCamera3
            // 
            this.tsmiCamera3.Checked = true;
            this.tsmiCamera3.CheckOnClick = true;
            this.tsmiCamera3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiCamera3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiReConnect3,
            this.tsmiProperty3,
            this.tsmiSnapImg3,
            this.tsmiContinues3});
            this.tsmiCamera3.Name = "tsmiCamera3";
            this.tsmiCamera3.Size = new System.Drawing.Size(107, 22);
            this.tsmiCamera3.Text = "相机3";
            // 
            // tsmiReConnect3
            // 
            this.tsmiReConnect3.Name = "tsmiReConnect3";
            this.tsmiReConnect3.Size = new System.Drawing.Size(124, 22);
            this.tsmiReConnect3.Text = "重连相机";
            this.tsmiReConnect3.Click += new System.EventHandler(this.tsmiReConnect3_Click);
            // 
            // tsmiProperty3
            // 
            this.tsmiProperty3.Name = "tsmiProperty3";
            this.tsmiProperty3.Size = new System.Drawing.Size(124, 22);
            this.tsmiProperty3.Text = "参数设置";
            this.tsmiProperty3.Click += new System.EventHandler(this.tsmiProperty3_Click);
            // 
            // tsmiSnapImg3
            // 
            this.tsmiSnapImg3.Name = "tsmiSnapImg3";
            this.tsmiSnapImg3.Size = new System.Drawing.Size(124, 22);
            this.tsmiSnapImg3.Text = "单帧采集";
            this.tsmiSnapImg3.Click += new System.EventHandler(this.tsmiSnapImg3_Click);
            // 
            // tsmiContinues3
            // 
            this.tsmiContinues3.Name = "tsmiContinues3";
            this.tsmiContinues3.Size = new System.Drawing.Size(124, 22);
            this.tsmiContinues3.Text = "连续采集";
            this.tsmiContinues3.Click += new System.EventHandler(this.tsmiContinues3_Click);
            // 
            // tsmiCamera4
            // 
            this.tsmiCamera4.Checked = true;
            this.tsmiCamera4.CheckOnClick = true;
            this.tsmiCamera4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiCamera4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiReConnect4,
            this.tsmiProperty4,
            this.tsmiSnapImg4,
            this.tsmiContinues4});
            this.tsmiCamera4.Name = "tsmiCamera4";
            this.tsmiCamera4.Size = new System.Drawing.Size(107, 22);
            this.tsmiCamera4.Text = "相机4";
            // 
            // tsmiReConnect4
            // 
            this.tsmiReConnect4.Name = "tsmiReConnect4";
            this.tsmiReConnect4.Size = new System.Drawing.Size(124, 22);
            this.tsmiReConnect4.Text = "重连相机";
            this.tsmiReConnect4.Click += new System.EventHandler(this.tsmiReConnect4_Click);
            // 
            // tsmiProperty4
            // 
            this.tsmiProperty4.Name = "tsmiProperty4";
            this.tsmiProperty4.Size = new System.Drawing.Size(124, 22);
            this.tsmiProperty4.Text = "参数设置";
            this.tsmiProperty4.Click += new System.EventHandler(this.tsmiProperty4_Click);
            // 
            // tsmiSnapImg4
            // 
            this.tsmiSnapImg4.Name = "tsmiSnapImg4";
            this.tsmiSnapImg4.Size = new System.Drawing.Size(124, 22);
            this.tsmiSnapImg4.Text = "单帧采集";
            this.tsmiSnapImg4.Click += new System.EventHandler(this.tsmiSnapImg4_Click);
            // 
            // tsmiContinues4
            // 
            this.tsmiContinues4.Name = "tsmiContinues4";
            this.tsmiContinues4.Size = new System.Drawing.Size(124, 22);
            this.tsmiContinues4.Text = "连续采集";
            this.tsmiContinues4.Click += new System.EventHandler(this.tsmiContinues4_Click);
            // 
            // tsmiImgAnaly
            // 
            this.tsmiImgAnaly.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDrawCircle,
            this.tsmiFindCircle,
            this.设置参数ToolStripMenuItem});
            this.tsmiImgAnaly.Name = "tsmiImgAnaly";
            this.tsmiImgAnaly.Size = new System.Drawing.Size(68, 21);
            this.tsmiImgAnaly.Text = "图像处理";
            // 
            // tsmiDrawCircle
            // 
            this.tsmiDrawCircle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDrawRec1,
            this.tsmiDrawRec2,
            this.tsmiDrawRec3,
            this.tsmiDrawRec4});
            this.tsmiDrawCircle.Name = "tsmiDrawCircle";
            this.tsmiDrawCircle.Size = new System.Drawing.Size(124, 22);
            this.tsmiDrawCircle.Text = "画区域";
            this.tsmiDrawCircle.Click += new System.EventHandler(this.tsmiDrawCircle_Click);
            // 
            // tsmiDrawRec1
            // 
            this.tsmiDrawRec1.Name = "tsmiDrawRec1";
            this.tsmiDrawRec1.Size = new System.Drawing.Size(107, 22);
            this.tsmiDrawRec1.Text = "窗口1";
            this.tsmiDrawRec1.Click += new System.EventHandler(this.tsmiDrawCircle1_Click);
            // 
            // tsmiDrawRec2
            // 
            this.tsmiDrawRec2.Name = "tsmiDrawRec2";
            this.tsmiDrawRec2.Size = new System.Drawing.Size(107, 22);
            this.tsmiDrawRec2.Text = "窗口2";
            this.tsmiDrawRec2.Click += new System.EventHandler(this.tsmiDrawCircle2_Click);
            // 
            // tsmiDrawRec3
            // 
            this.tsmiDrawRec3.Name = "tsmiDrawRec3";
            this.tsmiDrawRec3.Size = new System.Drawing.Size(107, 22);
            this.tsmiDrawRec3.Text = "窗口3";
            this.tsmiDrawRec3.Click += new System.EventHandler(this.tsmiDrawRec3_Click);
            // 
            // tsmiDrawRec4
            // 
            this.tsmiDrawRec4.Name = "tsmiDrawRec4";
            this.tsmiDrawRec4.Size = new System.Drawing.Size(107, 22);
            this.tsmiDrawRec4.Text = "窗口4";
            this.tsmiDrawRec4.Click += new System.EventHandler(this.tsmiDrawRec4_Click);
            // 
            // tsmiFindCircle
            // 
            this.tsmiFindCircle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFindCircle1,
            this.tsmiFindCircle2,
            this.tsmiFindCircle3,
            this.tsmiFindCircle4});
            this.tsmiFindCircle.Name = "tsmiFindCircle";
            this.tsmiFindCircle.Size = new System.Drawing.Size(124, 22);
            this.tsmiFindCircle.Text = "找区域";
            this.tsmiFindCircle.Click += new System.EventHandler(this.tsmiFindCircle_Click);
            // 
            // tsmiFindCircle1
            // 
            this.tsmiFindCircle1.Name = "tsmiFindCircle1";
            this.tsmiFindCircle1.Size = new System.Drawing.Size(107, 22);
            this.tsmiFindCircle1.Text = "窗口1";
            this.tsmiFindCircle1.Click += new System.EventHandler(this.tsmiFindCircle1_Click);
            // 
            // tsmiFindCircle2
            // 
            this.tsmiFindCircle2.Name = "tsmiFindCircle2";
            this.tsmiFindCircle2.Size = new System.Drawing.Size(107, 22);
            this.tsmiFindCircle2.Text = "窗口2";
            this.tsmiFindCircle2.Click += new System.EventHandler(this.tsmiFindCircle2_Click);
            // 
            // tsmiFindCircle3
            // 
            this.tsmiFindCircle3.Name = "tsmiFindCircle3";
            this.tsmiFindCircle3.Size = new System.Drawing.Size(107, 22);
            this.tsmiFindCircle3.Text = "窗口3";
            // 
            // tsmiFindCircle4
            // 
            this.tsmiFindCircle4.Name = "tsmiFindCircle4";
            this.tsmiFindCircle4.Size = new System.Drawing.Size(107, 22);
            this.tsmiFindCircle4.Text = "窗口4";
            // 
            // 设置参数ToolStripMenuItem
            // 
            this.设置参数ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiParaImgAnaly,
            this.tsmiParaResult,
            this.tsmiParaSave});
            this.设置参数ToolStripMenuItem.Name = "设置参数ToolStripMenuItem";
            this.设置参数ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.设置参数ToolStripMenuItem.Text = "设置参数";
            // 
            // tsmiParaImgAnaly
            // 
            this.tsmiParaImgAnaly.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.窗口1ToolStripMenuItem2,
            this.窗口2ToolStripMenuItem2});
            this.tsmiParaImgAnaly.Name = "tsmiParaImgAnaly";
            this.tsmiParaImgAnaly.Size = new System.Drawing.Size(148, 22);
            this.tsmiParaImgAnaly.Text = "图像处理参数";
            this.tsmiParaImgAnaly.Click += new System.EventHandler(this.tsmiParaImgAnaly_Click);
            // 
            // 窗口1ToolStripMenuItem2
            // 
            this.窗口1ToolStripMenuItem2.Name = "窗口1ToolStripMenuItem2";
            this.窗口1ToolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
            this.窗口1ToolStripMenuItem2.Text = "窗口1";
            // 
            // 窗口2ToolStripMenuItem2
            // 
            this.窗口2ToolStripMenuItem2.Name = "窗口2ToolStripMenuItem2";
            this.窗口2ToolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
            this.窗口2ToolStripMenuItem2.Text = "窗口2";
            // 
            // tsmiParaResult
            // 
            this.tsmiParaResult.Name = "tsmiParaResult";
            this.tsmiParaResult.Size = new System.Drawing.Size(148, 22);
            this.tsmiParaResult.Text = "结果处理参数";
            this.tsmiParaResult.Click += new System.EventHandler(this.tsmiParaResult_Click);
            // 
            // tsmiParaSave
            // 
            this.tsmiParaSave.Name = "tsmiParaSave";
            this.tsmiParaSave.Size = new System.Drawing.Size(148, 22);
            this.tsmiParaSave.Text = "存储设置";
            this.tsmiParaSave.Click += new System.EventHandler(this.tsmiParaSave_Click);
            // 
            // 网络连接ToolStripMenuItem
            // 
            this.网络连接ToolStripMenuItem.Name = "网络连接ToolStripMenuItem";
            this.网络连接ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.网络连接ToolStripMenuItem.Text = "连机设置";
            // 
            // hWindowControl2
            // 
            this.hWindowControl2.BackColor = System.Drawing.Color.Black;
            this.hWindowControl2.BorderColor = System.Drawing.Color.Black;
            this.hWindowControl2.ImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindowControl2.Location = new System.Drawing.Point(549, 6);
            this.hWindowControl2.Name = "hWindowControl2";
            this.hWindowControl2.Size = new System.Drawing.Size(536, 415);
            this.hWindowControl2.TabIndex = 4;
            this.hWindowControl2.WindowSize = new System.Drawing.Size(536, 415);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStart.Location = new System.Drawing.Point(10, 85);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(151, 95);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "运行程序";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStop.Location = new System.Drawing.Point(10, 196);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(151, 95);
            this.btnStop.TabIndex = 8;
            this.btnStop.Text = "停止程序";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnData
            // 
            this.btnData.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnData.Location = new System.Drawing.Point(10, 308);
            this.btnData.Name = "btnData";
            this.btnData.Size = new System.Drawing.Size(151, 95);
            this.btnData.TabIndex = 9;
            this.btnData.Text = "重启程序";
            this.btnData.UseVisualStyleBackColor = true;
            this.btnData.Click += new System.EventHandler(this.btnData_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnExit.Location = new System.Drawing.Point(10, 419);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(151, 95);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "退出程序";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4,
            this.toolStripStatusLabel5,
            this.toolStripStatusLabel6,
            this.toolStripStatusLabel7,
            this.toolStripStatusLabel8,
            this.toolStripStatusLabel9,
            this.toolStripStatusLabel10,
            this.toolStripStatusLabel11,
            this.toolStripStatusLabel12});
            this.statusStrip1.Location = new System.Drawing.Point(0, 945);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1257, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(78, 17);
            this.toolStripStatusLabel1.Text = "|相机1状态：";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel2.Text = "已连接|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(69, 17);
            this.toolStripStatusLabel3.Text = "|相机2状态:";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel4.Text = "已连接|";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(69, 17);
            this.toolStripStatusLabel5.Text = "|相机3状态:";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel6.Text = "已连接|";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(69, 17);
            this.toolStripStatusLabel7.Text = "|相机4状态:";
            // 
            // toolStripStatusLabel8
            // 
            this.toolStripStatusLabel8.Name = "toolStripStatusLabel8";
            this.toolStripStatusLabel8.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel8.Text = "已连接|";
            // 
            // toolStripStatusLabel9
            // 
            this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
            this.toolStripStatusLabel9.Size = new System.Drawing.Size(62, 17);
            this.toolStripStatusLabel9.Text = "|联机状态:";
            // 
            // toolStripStatusLabel10
            // 
            this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
            this.toolStripStatusLabel10.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel10.Text = "在线|";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(71, 17);
            this.toolStripStatusLabel11.Text = "|程序状态：";
            // 
            // toolStripStatusLabel12
            // 
            this.toolStripStatusLabel12.Name = "toolStripStatusLabel12";
            this.toolStripStatusLabel12.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel12.Text = "运行中|";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(163, 48);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1101, 919);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabPage1.Controls.Add(this.hWindowControl3);
            this.tabPage1.Controls.Add(this.hWindowControl4);
            this.tabPage1.Controls.Add(this.hWindowControl1);
            this.tabPage1.Controls.Add(this.hWindowControl2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1093, 893);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "图像显示";
            // 
            // hWindowControl3
            // 
            this.hWindowControl3.BackColor = System.Drawing.Color.Black;
            this.hWindowControl3.BorderColor = System.Drawing.Color.Black;
            this.hWindowControl3.ImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindowControl3.Location = new System.Drawing.Point(20, 440);
            this.hWindowControl3.Name = "hWindowControl3";
            this.hWindowControl3.Size = new System.Drawing.Size(507, 415);
            this.hWindowControl3.TabIndex = 5;
            this.hWindowControl3.WindowSize = new System.Drawing.Size(507, 415);
            // 
            // hWindowControl4
            // 
            this.hWindowControl4.BackColor = System.Drawing.Color.Black;
            this.hWindowControl4.BorderColor = System.Drawing.Color.Black;
            this.hWindowControl4.ImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindowControl4.Location = new System.Drawing.Point(549, 440);
            this.hWindowControl4.Name = "hWindowControl4";
            this.hWindowControl4.Size = new System.Drawing.Size(536, 415);
            this.hWindowControl4.TabIndex = 6;
            this.hWindowControl4.WindowSize = new System.Drawing.Size(536, 415);
            // 
            // btnTrigger
            // 
            this.btnTrigger.Location = new System.Drawing.Point(85, 20);
            this.btnTrigger.Name = "btnTrigger";
            this.btnTrigger.Size = new System.Drawing.Size(60, 24);
            this.btnTrigger.TabIndex = 13;
            this.btnTrigger.Text = "触发";
            this.btnTrigger.UseVisualStyleBackColor = true;
            // 
            // btnResult
            // 
            this.btnResult.Location = new System.Drawing.Point(85, 90);
            this.btnResult.Name = "btnResult";
            this.btnResult.Size = new System.Drawing.Size(60, 22);
            this.btnResult.TabIndex = 14;
            this.btnResult.Text = "结果";
            this.btnResult.UseVisualStyleBackColor = true;
            this.btnResult.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnCCDFinish
            // 
            this.btnCCDFinish.Location = new System.Drawing.Point(85, 54);
            this.btnCCDFinish.Name = "btnCCDFinish";
            this.btnCCDFinish.Size = new System.Drawing.Size(60, 22);
            this.btnCCDFinish.TabIndex = 15;
            this.btnCCDFinish.Text = "CCD结束";
            this.btnCCDFinish.UseVisualStyleBackColor = true;
            this.btnCCDFinish.Click += new System.EventHandler(this.button3_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1257, 53);
            this.toolStrip1.TabIndex = 19;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackColor = System.Drawing.Color.Lime;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(164, 50);
            this.toolStripButton1.Text = "采集一次";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Font = new System.Drawing.Font("宋体", 42F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(313, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1036, 56);
            this.label1.TabIndex = 20;
            this.label1.Text = "       套隔圈检测机";
            // 
            // btnConnectState
            // 
            this.btnConnectState.Location = new System.Drawing.Point(8, 19);
            this.btnConnectState.Name = "btnConnectState";
            this.btnConnectState.Size = new System.Drawing.Size(71, 92);
            this.btnConnectState.TabIndex = 22;
            this.btnConnectState.Text = "通信状态灯";
            this.btnConnectState.UseVisualStyleBackColor = true;
            this.btnConnectState.Click += new System.EventHandler(this.btnConnectState_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbPLCState);
            this.groupBox1.Controls.Add(this.btnConnectState);
            this.groupBox1.Controls.Add(this.btnTrigger);
            this.groupBox1.Controls.Add(this.btnResult);
            this.groupBox1.Controls.Add(this.btnCCDFinish);
            this.groupBox1.Location = new System.Drawing.Point(10, 520);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(151, 166);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // tbPLCState
            // 
            this.tbPLCState.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbPLCState.Location = new System.Drawing.Point(8, 118);
            this.tbPLCState.Name = "tbPLCState";
            this.tbPLCState.ReadOnly = true;
            this.tbPLCState.Size = new System.Drawing.Size(137, 35);
            this.tbPLCState.TabIndex = 24;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tbOKRate);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbTotalPec);
            this.groupBox2.Controls.Add(this.tbNGPec);
            this.groupBox2.Controls.Add(this.tbOKPec);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnClear);
            this.groupBox2.Location = new System.Drawing.Point(10, 692);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(151, 250);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(134, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 12);
            this.label7.TabIndex = 23;
            this.label7.Text = "%";
            // 
            // tbOKRate
            // 
            this.tbOKRate.Location = new System.Drawing.Point(52, 105);
            this.tbOKRate.Multiline = true;
            this.tbOKRate.Name = "tbOKRate";
            this.tbOKRate.ReadOnly = true;
            this.tbOKRate.Size = new System.Drawing.Size(76, 21);
            this.tbOKRate.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "   良率";
            // 
            // tbTotalPec
            // 
            this.tbTotalPec.Location = new System.Drawing.Point(52, 153);
            this.tbTotalPec.Name = "tbTotalPec";
            this.tbTotalPec.ReadOnly = true;
            this.tbTotalPec.Size = new System.Drawing.Size(93, 21);
            this.tbTotalPec.TabIndex = 20;
            // 
            // tbNGPec
            // 
            this.tbNGPec.Location = new System.Drawing.Point(52, 63);
            this.tbNGPec.Name = "tbNGPec";
            this.tbNGPec.ReadOnly = true;
            this.tbNGPec.Size = new System.Drawing.Size(93, 21);
            this.tbNGPec.TabIndex = 19;
            // 
            // tbOKPec
            // 
            this.tbOKPec.Location = new System.Drawing.Point(52, 20);
            this.tbOKPec.Name = "tbOKPec";
            this.tbOKPec.ReadOnly = true;
            this.tbOKPec.Size = new System.Drawing.Size(93, 21);
            this.tbOKPec.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "   ALL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "   NG";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "   OK";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(26, 200);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(102, 44);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "清除计数";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.帮助ToolStripMenuItem.Text = "帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1257, 967);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnData);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "深圳市佳康捷科技有限公司";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HalconDotNet.HWindowControl hWindowControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiCreateProject;
        private System.Windows.Forms.ToolStripMenuItem tsmiOpenProject;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveProject;
        private System.Windows.Forms.ToolStripMenuItem tsmiOpenImg;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveImg;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripMenuItem tsmiImgAnaly;
        private System.Windows.Forms.ToolStripMenuItem 相机ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera1;
        private System.Windows.Forms.ToolStripMenuItem tsmiReConnect1;
        private System.Windows.Forms.ToolStripMenuItem tsmiProperty1;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera2;
        private System.Windows.Forms.ToolStripMenuItem tsmiReConnect2;
        private System.Windows.Forms.ToolStripMenuItem tsmiProperty2;
        private HalconDotNet.HWindowControl hWindowControl2;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnData;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ToolStripMenuItem 网络连接ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel8;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel12;
        private System.Windows.Forms.ToolStripMenuItem tsmiSnapImg1;
        private System.Windows.Forms.ToolStripMenuItem tsmiContinues1;
        private System.Windows.Forms.ToolStripMenuItem tsmiSnapImg2;
        private System.Windows.Forms.ToolStripMenuItem tsmiContinues2;
        private System.Windows.Forms.ToolStripMenuItem tsmiImgShowCamera1;
        private System.Windows.Forms.ToolStripMenuItem tsmiImgShowCamera2;
        private System.Windows.Forms.ToolStripMenuItem tsmiImgCamera1;
        private System.Windows.Forms.ToolStripMenuItem tsmiImgCamera2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem tsmiDrawCircle;
        private System.Windows.Forms.ToolStripMenuItem tsmiFindCircle;
        private System.Windows.Forms.ToolStripMenuItem 设置参数ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiParaImgAnaly;
        private System.Windows.Forms.ToolStripMenuItem tsmiParaResult;
        private System.Windows.Forms.ToolStripMenuItem tsmiParaSave;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolStripMenuItem tsmiDrawRec1;
        private System.Windows.Forms.ToolStripMenuItem tsmiDrawRec2;
        private System.Windows.Forms.ToolStripMenuItem tsmiFindCircle1;
        private System.Windows.Forms.ToolStripMenuItem tsmiFindCircle2;
        private System.Windows.Forms.ToolStripMenuItem 窗口1ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 窗口2ToolStripMenuItem2;
        private System.Windows.Forms.Button btnTrigger;
        private System.Windows.Forms.Button btnResult;
        private System.Windows.Forms.Button btnCCDFinish;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private HalconDotNet.HWindowControl hWindowControl3;
        private HalconDotNet.HWindowControl hWindowControl4;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera3;
        private System.Windows.Forms.ToolStripMenuItem tsmiReConnect3;
        private System.Windows.Forms.ToolStripMenuItem tsmiProperty3;
        private System.Windows.Forms.ToolStripMenuItem tsmiSnapImg3;
        private System.Windows.Forms.ToolStripMenuItem tsmiContinues3;
        private System.Windows.Forms.ToolStripMenuItem tsmiCamera4;
        private System.Windows.Forms.ToolStripMenuItem tsmiReConnect4;
        private System.Windows.Forms.ToolStripMenuItem tsmiProperty4;
        private System.Windows.Forms.ToolStripMenuItem tsmiSnapImg4;
        private System.Windows.Forms.ToolStripMenuItem tsmiContinues4;
        private System.Windows.Forms.ToolStripMenuItem tsmiDrawRec3;
        private System.Windows.Forms.ToolStripMenuItem tsmiDrawRec4;
        private System.Windows.Forms.ToolStripMenuItem tsmiFindCircle3;
        private System.Windows.Forms.ToolStripMenuItem tsmiFindCircle4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnectState;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbOKRate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbTotalPec;
        private System.Windows.Forms.TextBox tbNGPec;
        private System.Windows.Forms.TextBox tbOKPec;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox tbPLCState;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
    }
}

