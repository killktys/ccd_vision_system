﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace C17Software
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Help_Load(object sender, EventArgs e)
        {
            //建立Word类的实例,缺点:不能正确读取表格,图片等等的显示
            var app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = null;
            object missing = System.Reflection.Missing.Value;

            object FileName = Application.StartupPath + "\\help.docx";
            object readOnly = false;
            object isVisible = true;
            object index = 0;
            try
            {
                doc = app.Documents.Open(ref FileName, ref missing, ref readOnly,
                 ref missing, ref missing, ref missing, ref missing, ref missing,
                 ref missing, ref missing, ref missing, ref isVisible, ref missing,
                 ref missing, ref missing, ref missing);

                doc.ActiveWindow.Selection.WholeStory();
                doc.ActiveWindow.Selection.Copy();
                //从剪切板获取数据
                IDataObject data = Clipboard.GetDataObject();
                this.richTextBox1.Paste();// = data.GetData(DataFormats.Text).ToString();
                this.richTextBox1.ReadOnly = true;

            }
            finally
            {
                if (doc != null)
                {
                    doc.Close(ref missing, ref missing, ref missing);
                    doc = null;
                }

                if (app != null)
                {
                    app.Quit(ref missing, ref missing, ref missing);
                    app = null;
                }
            }

        }
    }
}
