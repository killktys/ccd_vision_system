﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using csFileClass;
namespace C17Software
{
    public partial class ResultAnalyForm : Form
    {
        private FileClass m_fileClass = new FileClass();
        private double Delta = 1.0;
        public ResultAnalyForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string _strFile= "";
            double[] _dlResult = new double[8];
            _dlResult[0] = double.Parse(tbDistance1.Text);
            _dlResult[1] = double.Parse(tbDistance11.Text);
            _dlResult[2] = double.Parse(tbDistance2.Text);
            _dlResult[3] = double.Parse(tbDistance21.Text);
            _dlResult[4] = double.Parse(tbDistance3.Text);
            _dlResult[5] = double.Parse(tbDistance31.Text);
            _dlResult[6] = double.Parse(tbDistance4.Text);
            _dlResult[7] = double.Parse(tbDistance41.Text);
            switch (cbCameraName.Text)
            {
                case "相机1":
                    _strFile = "\\MeasurePara1.ini";
                    WriteCircleP(_dlResult, _strFile);
                    break;
                case "相机2":
                      _strFile = "\\MeasurePara2.ini";
                    WriteCircleP(_dlResult, _strFile);
                    break;
                case "相机3":
                    _strFile = "\\MeasurePara3.ini";
                    WriteCircleP(_dlResult, _strFile);
                    break;
                case "相机4":
                    _strFile = "\\MeasurePara4.ini";
                    WriteCircleP(_dlResult, _strFile);
                    break;
            }
          
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ResultAnalyForm_Load(object sender, EventArgs e)
        {
            double[] _dlCircleP = ReadMeasureP("\\MeasurePara1.ini");
            
            tbDistance1.Text = (_dlCircleP[0] * Delta).ToString();
            tbDistance11.Text = (_dlCircleP[1] * Delta).ToString();

            tbDistance2.Text = (_dlCircleP[2] * Delta).ToString();
            tbDistance21.Text = (_dlCircleP[3] * Delta).ToString();

            tbDistance3.Text = (_dlCircleP[4] * Delta).ToString();
            tbDistance31.Text = (_dlCircleP[5] * Delta).ToString();

            tbDistance4.Text = (_dlCircleP[6] * Delta).ToString();
            tbDistance41.Text = (_dlCircleP[7] * Delta).ToString();
        }
        private double[] ReadMeasureP(string _strFile)
        {
            string[] _strMeasureP = m_fileClass.ReadIni(Application.StartupPath + _strFile);

            double[] _dlCircleP = new double[_strMeasureP.Length];

            for (int i = 0; i < _strMeasureP.Length; i++)
            {
                _dlCircleP[i] = double.Parse(_strMeasureP[i]);
            }
            return _dlCircleP;
        }
        private void WriteCircleP(double[] _dlResult, string _strFile)
        {
            string str = _dlResult[0].ToString() + "," + _dlResult[1].ToString() + "," + _dlResult[2].ToString()
                + "," + _dlResult[3].ToString() + "," + _dlResult[4].ToString() + "," + _dlResult[5].ToString()
                + "," + _dlResult[6].ToString() + "," + _dlResult[7].ToString();
            m_fileClass.WriteIni(str, Application.StartupPath + _strFile);
        }

        private void cbCameraName_SelectedIndexChanged(object sender, EventArgs e)
        {
            double[] _dlCircleP;
            switch (cbCameraName.Text)
            {
                case "相机1":
                    _dlCircleP = ReadMeasureP("\\MeasurePara1.ini");

                    tbDistance1.Text = (_dlCircleP[0] * Delta).ToString();
                    tbDistance11.Text = (_dlCircleP[1] * Delta).ToString();

                    tbDistance2.Text = (_dlCircleP[2] * Delta).ToString();
                    tbDistance21.Text = (_dlCircleP[3] * Delta).ToString();

                    tbDistance3.Text = (_dlCircleP[4] * Delta).ToString();
                    tbDistance31.Text = (_dlCircleP[5] * Delta).ToString();

                    tbDistance4.Text = (_dlCircleP[6] * Delta).ToString();
                    tbDistance41.Text = (_dlCircleP[7] * Delta).ToString();
                    break;
                case "相机2":
                    _dlCircleP = ReadMeasureP("\\MeasurePara2.ini");

                    tbDistance1.Text = (_dlCircleP[0] * Delta).ToString();
                    tbDistance11.Text = (_dlCircleP[1] * Delta).ToString();

                    tbDistance2.Text = (_dlCircleP[2] * Delta).ToString();
                    tbDistance21.Text = (_dlCircleP[3] * Delta).ToString();

                    tbDistance3.Text = (_dlCircleP[4] * Delta).ToString();
                    tbDistance31.Text = (_dlCircleP[5] * Delta).ToString();

                    tbDistance4.Text = (_dlCircleP[6] * Delta).ToString();
                    tbDistance41.Text = (_dlCircleP[7] * Delta).ToString();
                    break;
                case "相机3":
                    _dlCircleP = ReadMeasureP("\\MeasurePara3.ini");

                    tbDistance1.Text = (_dlCircleP[0] * Delta).ToString();
                    tbDistance11.Text = (_dlCircleP[1] * Delta).ToString();

                    tbDistance2.Text = (_dlCircleP[2] * Delta).ToString();
                    tbDistance21.Text = (_dlCircleP[3] * Delta).ToString();

                    tbDistance3.Text = (_dlCircleP[4] * Delta).ToString();
                    tbDistance31.Text = (_dlCircleP[5] * Delta).ToString();

                    tbDistance4.Text = (_dlCircleP[6] * Delta).ToString();
                    tbDistance41.Text = (_dlCircleP[7] * Delta).ToString();
                    break;
                case "相机4":
                    _dlCircleP = ReadMeasureP("\\MeasurePara4.ini");

                    tbDistance1.Text = (_dlCircleP[0] * Delta).ToString();
                    tbDistance11.Text = (_dlCircleP[1] * Delta).ToString();

                    tbDistance2.Text = (_dlCircleP[2] * Delta).ToString();
                    tbDistance21.Text = (_dlCircleP[3] * Delta).ToString();

                    tbDistance3.Text = (_dlCircleP[4] * Delta).ToString();
                    tbDistance31.Text = (_dlCircleP[5] * Delta).ToString();

                    tbDistance4.Text = (_dlCircleP[6] * Delta).ToString();
                    tbDistance41.Text = (_dlCircleP[7] * Delta).ToString();
                    break;
            }
        }
    }
}
