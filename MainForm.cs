﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HalconDotNet;
using csImagingCamera;
//using csFindCircle;
using System.IO;
using csFileClass;
//using csPattern;
//using csBeifuConnect;
using System.Threading;
using csBlob;
using csDisplayPara;
using csDistance;
using csPlcConnect;

namespace C17Software
{
    public partial class MainForm : Form
    {
        private static HWindow WinID1 = new HWindow();
        private static HWindow WinID2 = new HWindow();
        private static HWindow WinID3 = new HWindow();
        private static HWindow WinID4 = new HWindow();

        private static DataTable dt = new DataTable();
        private static Control FormHandle;

        private Camera camera1 = new Camera();
        private Camera camera2 = new Camera();
        private Camera camera3 = new Camera();
        private Camera camera4 = new Camera();

        private string m_strDate1;
        private string m_strDate2;
        private string m_strDate3;
        private string m_strDate4;

        private bool m_bSnapOnce = false;
        //private FindCircle m_findCirle1 = new FindCircle();
        //private FindCircle m_findCirle2 = new FindCircle();
        private Blob m_blob1 = new Blob();
        private Blob m_blob2 = new Blob();
        private Blob m_blob3 = new Blob();
        private Blob m_blob4 = new Blob();

        private Distance m_distance1 = new Distance();
        private Distance m_distance2 = new Distance();
        private Distance m_distance3 = new Distance();
        private Distance m_distance4 = new Distance();

        private DisplayPara m_displayPara = new DisplayPara();
        //private Pattern m_pattern1 = new Pattern();
        //private Pattern m_pattern2 = new Pattern();
   

        HObject m_hbImg1 = new HObject();
        HObject m_hbImg2 = new HObject();
        HObject m_hbImg3 = new HObject();
        HObject m_hbImg4 = new HObject();

        HTuple m_htModelID1 = new HTuple();
        HTuple m_htModelID2 = new HTuple();
        HTuple m_htModelID3 = new HTuple();
        HTuple m_htModelID4 = new HTuple();

        private Thread m_AnalyThread1;
        private Thread m_AnalyThread2;
        private Thread m_AnalyThread3;
        private Thread m_AnalyThread4;

        private Thread m_PlcThread;

        private bool m_bAnalyThread1;
        private bool m_bAnalyThread2;
        private bool m_bAnalyThread3;
        private bool m_bAnalyThread4;

        private bool m_bPlcThread;

        private ManualResetEvent m_AnalyEvent1 = new ManualResetEvent(false);
        private ManualResetEvent m_AnalyEvent2 = new ManualResetEvent(false);
        private ManualResetEvent m_AnalyEvent3 = new ManualResetEvent(false);
        private ManualResetEvent m_AnalyEvent4 = new ManualResetEvent(false);
     

        private ManualResetEvent m_AnalyFinishEvent1 = new ManualResetEvent(false);
        private ManualResetEvent m_AnalyFinishEvent2 = new ManualResetEvent(false);
        private ManualResetEvent m_AnalyFinishEvent3 = new ManualResetEvent(false);
        private ManualResetEvent m_AnalyFinishEvent4 = new ManualResetEvent(false);

        private static bool m_bAnaly1;
        private static bool m_bAnaly2;
        private static bool m_bAnaly3;
        private static bool m_bAnaly4;

        private static bool m_bAnalyFinish1 = false;
        private static bool m_bAnalyFinish2 = false;
        private static bool m_bAnalyFinish3 = false;
        private static bool m_bAnalyFinish4 = false;

        private FileClass fc = new FileClass();

        private PLCConnect m_plc = new PLCConnect();
        string m_strPlcIp = "172.168.250.80";
        string m_strPlcPort = "9600";
        int m_iPcIp = 208;
        int m_iPlcIp = 80;

        private static UInt32 m_uiOKPec = 0;
        private static UInt32 m_uiNGPec = 0;
        private static UInt32 m_uiTotalPec = 0;
        private static double m_dlOKRate = 0;

        public MainForm()
        {
            InitializeComponent();
        }
      
        private void MainForm_Load(object sender, EventArgs e)
        {
            InitHalcon();
            
             string _strFileName = "D:\\Database\\" + DateTime.Now.Year.ToString() +
                DateTime.Now.Month.ToString() + "\\";
            if (System.IO.Directory.Exists(_strFileName) == false)
            {
                System.IO.Directory.CreateDirectory(_strFileName);
            }
            string _strCsvName = _strFileName + DateTime.Now.Day.ToString() + ".csv";
            if (System.IO.File.Exists(_strCsvName) == false)
            {
                DataColumn dc = new DataColumn("编号");
                dt.Columns.Add(dc);
                dc = new DataColumn("夹具号");
                dt.Columns.Add(dc);
                dc = new DataColumn("检测结果");
                dt.Columns.Add(dc);
                dc = new DataColumn("相机号");
                dt.Columns.Add(dc);
                dc = new DataColumn("对应时间");
                dt.Columns.Add(dc);
            }
            else
            {
                dt = fc.ReadCSV(_strCsvName);

            }

            //dgvData.DataSource = dt;

            camera1.hWindow = WinID1;
            camera2.hWindow = WinID2;
            camera3.hWindow = WinID3;
            camera4.hWindow = WinID4;

            FormHandle = this;

            CheckForIllegalCrossThreadCalls = false;

            InitCamera();
            
            InitConnect(m_strPlcIp, m_strPlcPort, m_iPcIp, m_iPlcIp);
           
        }
       
        private void InitConnect(string _strPlcIp,string _strPlcPort,int _iPcIp,int _iPlcip)
        {
            m_plc.Ip = _strPlcIp;
            m_plc.Port = _strPlcPort;
            if (m_plc.DataStreamFound())
            {               
                if (m_plc.ShakeHands(_iPcIp, _iPlcip))
                {
                    MessageBox.Show("Success Hand");
                }
                else
                {
                    MessageBox.Show("Failed Hand");
                }
            }
            else
            {
                MessageBox.Show("Failed Connected");
                this.Close();
            }

           
        }
        #region "相机设置"
        private void InitHalcon()
        {
            WinID1 = hWindowControl1.HalconWindow;
            WinID2 = hWindowControl2.HalconWindow;
            WinID3 = hWindowControl3.HalconWindow;
            WinID4 = hWindowControl4.HalconWindow;
        }
        private void InitCamera()
        {
            HTuple width, height;
            
            string str = Application.StartupPath + "\\1.bmp";
            m_hbImg1.Dispose();
            HOperatorSet.ReadImage(out m_hbImg1, str);
            HOperatorSet.GetImageSize(m_hbImg1, out width, out height);
            WinID1.SetPart(0, 0, height, width);
            WinID1.DispObj(m_hbImg1);           
           
            m_hbImg2.Dispose();
            HOperatorSet.ReadImage(out m_hbImg2, str);
            HOperatorSet.GetImageSize(m_hbImg2, out width, out height);
            WinID2.SetPart(0, 0, height, width);
            WinID2.DispObj(m_hbImg2);

            m_hbImg3.Dispose();
            HOperatorSet.ReadImage(out m_hbImg3, str);
            HOperatorSet.GetImageSize(m_hbImg3, out width, out height);
            WinID3.SetPart(0, 0, height, width);
            WinID3.DispObj(m_hbImg3);

            m_hbImg4.Dispose();
            HOperatorSet.ReadImage(out m_hbImg4, str);
            HOperatorSet.GetImageSize(m_hbImg4, out width, out height);
            WinID4.SetPart(0, 0, height, width);
            WinID4.DispObj(m_hbImg4);

            camera1.InitCamera("device1.xml");
            camera2.InitCamera("device2.xml");
            camera3.InitCamera("device3.xml");
            camera4.InitCamera("device4.xml");      
        }

        private void tsmiReConnect1_Click(object sender, EventArgs e)
        {
            string str = "device1.xml";
            camera1.ReConnectCamera(str);
        }

        private void tsmiProperty1_Click(object sender, EventArgs e)
        {
            string str = "device1.xml";
            camera1.Property(str);
        }

        private void tsmiSnapImg1_Click(object sender, EventArgs e)
        {
            m_hbImg1.Dispose();
            camera1.SnapImage();

            Thread.Sleep(200);
            camera1.manualcamera = true;
            m_hbImg1 = camera1.Image;      
                                                                                                                                                                                                                                                                                                      
        }

        private void tsmiContinues1_Click(object sender, EventArgs e)
        {
            camera1.ContinueCamera();
            
        }

        private void tsmiReConnect2_Click(object sender, EventArgs e)
        {
            string str = "device2.xml";
            camera2.ReConnectCamera(str);
        }

        private void tsmiProperty2_Click(object sender, EventArgs e)
        {
            string str = "device2.xml";
            camera2.Property(str);   
        }

        private void tsmiSnapImg2_Click(object sender, EventArgs e)
        {
            m_hbImg2.Dispose();
            camera2.SnapImage();
            Thread.Sleep(200);
            m_hbImg2 = camera2.Image;
            camera2.manualcamera = true;
            
           
        }

        private void tsmiContinues2_Click(object sender, EventArgs e)
        {
            camera2.ContinueCamera();
            m_hbImg2 = camera2.Image;
        }

        private void tsmiReConnect3_Click(object sender, EventArgs e)
        {
            string str = "device3.xml";
            camera3.ReConnectCamera(str);
        }

        private void tsmiProperty3_Click(object sender, EventArgs e)
        {
            string str = "device3.xml";
            camera3.Property(str);   
        }

        private void tsmiSnapImg3_Click(object sender, EventArgs e)
        {
            m_hbImg3.Dispose();
            camera3.SnapImage();
            Thread.Sleep(200);
            m_hbImg3 = camera3.Image;
            camera3.manualcamera = true;
        }

        private void tsmiContinues3_Click(object sender, EventArgs e)
        {
            camera3.ContinueCamera();
            m_hbImg3 = camera3.Image;
        }

        private void tsmiReConnect4_Click(object sender, EventArgs e)
        {
            string str = "device4.xml";
            camera4.ReConnectCamera(str);
        }

        private void tsmiProperty4_Click(object sender, EventArgs e)
        {
            string str = "device4.xml";
            camera4.Property(str);   
        }

        private void tsmiSnapImg4_Click(object sender, EventArgs e)
        {
            m_hbImg4.Dispose();
            camera4.SnapImage();
            Thread.Sleep(200);
            m_hbImg4 = camera4.Image;
            camera4.manualcamera = true;
        }

        private void tsmiContinues4_Click(object sender, EventArgs e)
        {
            camera4.ContinueCamera();
            m_hbImg4 = camera4.Image;
        }
        private void CloseCamera()
        {
            camera1.StopCamera();
            camera2.StopCamera();
            camera3.StopCamera();
            camera4.StopCamera();

            camera1.DestroyCamera();
            camera2.DestroyCamera();
            camera3.DestroyCamera();
            camera4.DestroyCamera();
        }
        #endregion
        private void btnStart_Click(object sender, EventArgs e)
        {
            camera1.manualcamera = false;
            camera2.manualcamera = false;
            camera3.manualcamera = false;
            camera4.manualcamera = false;
                btnStart.Enabled = false;
                btnStop.Enabled = true;


                m_bPlcThread = true;
                m_PlcThread = new Thread(new ThreadStart(ThreadPLC));
                m_PlcThread.Start();
                timer1.Enabled = true;
          
        }
       
    

        private void ThreadPLC()
        {
            MainForm fm = (MainForm)FormHandle;
            int _iIndex = 0;
            string str;
            while (m_bPlcThread)
            {
                try
                {
                    str = "";
                    str = m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 200);
                    if (str == "" || str == null)
                        m_iTigger = 0;
                    else
                        m_iTigger = int.Parse(str);

                    // m_iTigger=int.Parse(m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 200));
                    if (m_bSnapOnce || m_iTigger == 1 && (m_iOldTrigger == 0))
                    {


                        m_iTigger = 0;


                        m_bAnaly1 = false;
                        m_bAnaly2 = false;
                        m_bAnaly3 = false;
                        m_bAnaly4 = false;

                        m_bAnalyFinish1 = false;
                        m_bAnalyFinish2 = false;
                        m_bAnalyFinish3 = false;
                        m_bAnalyFinish4 = false;

                        camera1.SnapImage();
                        camera2.SnapImage();
                        camera3.SnapImage();
                        camera4.SnapImage();

                        m_bAnalyThread1 = true;
                        m_AnalyThread1 = new Thread(new ThreadStart(ThreadSub1));
                        m_AnalyThread1.Start();

                        m_bAnalyThread2 = true;
                        m_AnalyThread2 = new Thread(new ThreadStart(ThreadSub2));
                        m_AnalyThread2.Start();

                        m_bAnalyThread3 = true;
                        m_AnalyThread3 = new Thread(new ThreadStart(ThreadSub3));
                        m_AnalyThread3.Start();

                        m_bAnalyThread4 = true;
                        m_AnalyThread4 = new Thread(new ThreadStart(ThreadSub4));
                        m_AnalyThread4.Start();

                        if (m_AnalyFinishEvent1.WaitOne(1300, false))
                        {
                            _iIndex = dt.Rows.Count;
                            //dt.Rows.Add(_iIndex.ToString(), Plc.ByteClamp.ToString(), m_bAnaly1.ToString(), "Camera1", m_strDate1);

                        }
                        m_AnalyFinishEvent1.Reset();
                        if (m_AnalyFinishEvent2.WaitOne(1300, false))
                        {
                            _iIndex = dt.Rows.Count;
                            // dt.Rows.Add(_iIndex.ToString(), Plc.ByteClamp.ToString(), m_bAnaly1.ToString(), "Camera2", m_strDate2);
                        }
                        m_AnalyFinishEvent2.Reset();

                        if (m_AnalyFinishEvent3.WaitOne(1300, false))
                        {
                            _iIndex = dt.Rows.Count;
                            //dt.Rows.Add(_iIndex.ToString(), Plc.ByteClamp.ToString(), m_bAnaly1.ToString(), "Camera1", m_strDate1);

                        }
                        m_AnalyFinishEvent3.Reset();
                        if (m_AnalyFinishEvent4.WaitOne(1300, false))
                        {
                            _iIndex = dt.Rows.Count;
                            // dt.Rows.Add(_iIndex.ToString(), Plc.ByteClamp.ToString(), m_bAnaly1.ToString(), "Camera2", m_strDate2);
                        }
                        m_AnalyFinishEvent4.Reset();

                        if (m_bAnaly1 && m_bAnaly2)
                        {
                            m_uiOKPec++;
                            m_uiTotalPec++;
                            m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 210, 1);
                            //Plc.BoolResult = true;
                        }
                        else
                        {
                            m_uiNGPec++;
                            m_uiTotalPec++;
                            m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 210, 2);
                            //Plc.BoolResult = false;
                        }
                        if (m_bAnaly3 && m_bAnaly4)
                        {
                            m_uiOKPec++;
                            m_uiTotalPec++;
                            m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 220, 1);
                        }
                        else
                        {
                            m_uiNGPec++;
                            m_uiTotalPec++;
                            m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 220, 2);
                        }
                        m_dlOKRate = ((m_uiOKPec * 1.0000) / m_uiTotalPec) * 100;

                        m_bSnapOnce = false;

                    }
                    //m_iOldTrigger = int.Parse(m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 200));
                    //m_iOldTrigger = m_iTigger;
                    //    }
                    Thread.Sleep(800);
                }
                catch
                { }
                
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
            if (m_AnalyThread1 != null)
            {
                if (m_AnalyThread1.IsAlive)
                {
                    m_bAnalyThread1 = false;
                    m_AnalyThread1.Abort();
                }
                
            }
            if (m_AnalyThread2 != null)
            {
                if (m_AnalyThread2.IsAlive)
                {
                    m_bAnalyThread2 = false;
                    m_AnalyThread2.Abort();
                }
               
            }
            if (m_AnalyThread3 != null)
            {
                if (m_AnalyThread3.IsAlive)
                {
                    m_bAnalyThread3 = false;
                    m_AnalyThread3.Abort();
                }

            }
            if (m_AnalyThread4 != null)
            {
                if (m_AnalyThread4.IsAlive)
                {
                    m_bAnalyThread4 = false;
                    m_AnalyThread4.Abort();
                }

            }
           
            if (m_PlcThread != null)
            {
                if (m_PlcThread.IsAlive)
                {
                    m_bPlcThread = false;
                    m_PlcThread.Abort();
                }
                
            }
            m_plc.DataStreamClose();
            CloseCamera();
            
        }

        private void tsmiCreateProject_Click(object sender, EventArgs e)
        {

        }

        private void tsmiOpenProject_Click(object sender, EventArgs e)
        {

        }

        private void tsmiSaveProject_Click(object sender, EventArgs e)
        {

        }
        private HObject OpenImage(HWindow _hwWinID)
        {
            HObject _hbImage = new HObject();
            openFileDialog1.Title = "请选择一张图片:";
            openFileDialog1.Filter = "图片(bmp)|*.bmp|所有文件|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                string str = openFileDialog1.FileName;
                HTuple width, height;
                try
                {
                    _hbImage.Dispose();
                    HOperatorSet.ReadImage(out _hbImage, str);
                    HOperatorSet.GetImageSize(_hbImage, out width, out height);
                    _hwWinID.SetPart(0, 0, height -1, width - 1);
                    _hwWinID.DispObj(_hbImage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("加载图片失败");
                }
            }
            return _hbImage;
        }
        private void tsmiImgShowCamera1_Click(object sender, EventArgs e)
        {
            camera1.SnapImage();
            m_hbImg1 = OpenImage(WinID1);
        }

        private void tsmiImgShowCamera2_Click(object sender, EventArgs e)
        {
            camera2.SnapImage();
            m_hbImg2 = OpenImage(WinID2);
        }

      
        private void SaveImage(HObject _hbImg)
        {
            
            saveFileDialog1.Filter = "图片(bmp)|*.bmp|所有文件|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string str = saveFileDialog1.FileName;
                HOperatorSet.WriteImage(_hbImg, "bmp", 0, str);
            }
            
        }
        private void tsmiImgCamera1_Click(object sender, EventArgs e)
        {
            camera1.SnapImage();
            SaveImage(camera1.Image);         
            
        }

        private void tsmiImgCamera2_Click(object sender, EventArgs e)
        {
            camera2.SnapImage();
            SaveImage(camera2.Image);
        }

     

        private void tsmiDrawCircle_Click(object sender, EventArgs e)
        {

        }

        private void tsmiFindCircle_Click(object sender, EventArgs e)
        {

        }

        private void tsmiParaImgAnaly_Click(object sender, EventArgs e)
        {

        }

        private void tsmiParaResult_Click(object sender, EventArgs e)
        {
            ResultAnalyForm rf = new ResultAnalyForm();
            rf.ShowDialog();
        }

        private void tsmiParaSave_Click(object sender, EventArgs e)
        {

        }
        private void AnalyResult(double[] _dlMeasureDist, HWindow _hwWinID,bool _bResult,
            HTuple _htPos, string _strClamb)           
        {            
            if (!_bResult)
            {                
                m_displayPara.set_display_font(_hwWinID, 30, "mono", "true", "false");
                HOperatorSet.SetLineWidth(_hwWinID, 3);
                m_displayPara.SetLine("red", "margin", _hwWinID);
                HOperatorSet.DispCross(_hwWinID, _dlMeasureDist[0], _dlMeasureDist[1], 20, 0);
                HOperatorSet.DispCross(_hwWinID, _dlMeasureDist[2], _dlMeasureDist[3], 20, 0);
                HOperatorSet.SetTposition(_hwWinID, _htPos, 0);
                HOperatorSet.WriteString(_hwWinID, "距离=" + Math.Round(_dlMeasureDist[4],3).ToString());
                
            }
            else
            {
                
                m_displayPara.set_display_font(_hwWinID, 30, "mono", "true", "false");
                HOperatorSet.SetLineWidth(_hwWinID, 3);
                m_displayPara.SetLine("green", "margin", _hwWinID);
                HOperatorSet.DispCross(_hwWinID, _dlMeasureDist[0], _dlMeasureDist[1], 20, 0);
                HOperatorSet.DispCross(_hwWinID, _dlMeasureDist[2], _dlMeasureDist[3], 20, 0);
                HOperatorSet.SetTposition(_hwWinID, _htPos, 0);
                HOperatorSet.WriteString(_hwWinID, "距离=" + Math.Round(_dlMeasureDist[4],3).ToString());
               
            }
        }
        private void DispForm(HWindow _hwWinID,bool _bResult)
        {
            if (_bResult)
            {
                HOperatorSet.SetLineWidth(_hwWinID, 3);
                m_displayPara.SetLine("green", "margin", _hwWinID);
               
                
            }
            else
            {
                HOperatorSet.SetLineWidth(_hwWinID, 3);
                m_displayPara.SetLine("red", "margin", _hwWinID);
            }
            HOperatorSet.DispRectangle1(_hwWinID, 0, 0, 1944, 2599);
            HOperatorSet.SetTposition(_hwWinID, 1800, 2400);
        }
        private void tsmiDrawCircle1_Click(object sender, EventArgs e)
        {
            double[] _dlRectangle2Para = new double[5];
            double[] _dlMeasureDist = new double[5];
            HOperatorSet.SetColor(WinID1, "red");
            string _strFile = "";
            _dlRectangle2Para = m_distance1.DrawDistanceRec(WinID1);
            _strFile = Application.StartupPath +"\\Rectangle11.ini";
            m_distance1.WritePara(_strFile, _dlRectangle2Para);

            _dlRectangle2Para = m_distance1.DrawDistanceRec(WinID1);
            _strFile = Application.StartupPath + "\\Rectangle12.ini";
            m_distance1.WritePara(_strFile, _dlRectangle2Para);

            _dlRectangle2Para = m_distance1.DrawDistanceRec(WinID1);
            _strFile = Application.StartupPath + "\\Rectangle13.ini";
            m_distance1.WritePara(_strFile, _dlRectangle2Para);

            _dlRectangle2Para = m_distance1.DrawDistanceRec(WinID1);
            _strFile = Application.StartupPath + "\\Rectangle14.ini";
            m_distance1.WritePara(_strFile, _dlRectangle2Para);          
          
        }

        private void tsmiDrawCircle2_Click(object sender, EventArgs e)
        {
            double[] _dlRectangle2Para = new double[5];
            double[] _dlMeasureDist = new double[5];
            string _strFile = "";
            HOperatorSet.SetColor(WinID2, "red");
            _dlRectangle2Para = m_distance2.DrawDistanceRec(WinID2);
            _strFile = Application.StartupPath + "\\Rectangle21.ini";
            m_distance2.WritePara(_strFile, _dlRectangle2Para);

            _dlRectangle2Para = m_distance2.DrawDistanceRec(WinID2);
            _strFile = Application.StartupPath + "\\Rectangle22.ini";
            m_distance2.WritePara(_strFile, _dlRectangle2Para);

            _dlRectangle2Para = m_distance2.DrawDistanceRec(WinID2);
            _strFile = Application.StartupPath + "\\Rectangle23.ini";
            m_distance2.WritePara(_strFile, _dlRectangle2Para);

            _dlRectangle2Para = m_distance2.DrawDistanceRec(WinID2);
            _strFile = Application.StartupPath + "\\Rectangle24.ini";
            m_distance2.WritePara(_strFile, _dlRectangle2Para);          
          
        }
        private void tsmiDrawRec3_Click(object sender, EventArgs e)
        {
            double[] _dlRectangle3Para = new double[5];
            double[] _dlMeasureDist = new double[5];
            string _strFile = "";
            HOperatorSet.SetColor(WinID3, "red");
            _dlRectangle3Para = m_distance3.DrawDistanceRec(WinID3);
            _strFile = Application.StartupPath + "\\Rectangle31.ini";
            m_distance3.WritePara(_strFile, _dlRectangle3Para);

            _dlRectangle3Para = m_distance3.DrawDistanceRec(WinID3);
            _strFile = Application.StartupPath + "\\Rectangle32.ini";
            m_distance3.WritePara(_strFile, _dlRectangle3Para);

            _dlRectangle3Para = m_distance3.DrawDistanceRec(WinID3);
            _strFile = Application.StartupPath + "\\Rectangle33.ini";
            m_distance3.WritePara(_strFile, _dlRectangle3Para);

            _dlRectangle3Para = m_distance3.DrawDistanceRec(WinID3);
            _strFile = Application.StartupPath + "\\Rectangle34.ini";
            m_distance3.WritePara(_strFile, _dlRectangle3Para);    
        }

        private void tsmiDrawRec4_Click(object sender, EventArgs e)
        {
            double[] _dlRectangle4Para = new double[5];
            double[] _dlMeasureDist = new double[5];
            string _strFile = "";
            HOperatorSet.SetColor(WinID4, "red");
            _dlRectangle4Para = m_distance4.DrawDistanceRec(WinID4);
            _strFile = Application.StartupPath + "\\Rectangle41.ini";
            m_distance4.WritePara(_strFile, _dlRectangle4Para);

            _dlRectangle4Para = m_distance4.DrawDistanceRec(WinID4);
            _strFile = Application.StartupPath + "\\Rectangle42.ini";
            m_distance4.WritePara(_strFile, _dlRectangle4Para);

            _dlRectangle4Para = m_distance4.DrawDistanceRec(WinID4);
            _strFile = Application.StartupPath + "\\Rectangle43.ini";
            m_distance4.WritePara(_strFile, _dlRectangle4Para);

            _dlRectangle4Para = m_distance4.DrawDistanceRec(WinID4);
            _strFile = Application.StartupPath + "\\Rectangle44.ini";
            m_distance4.WritePara(_strFile, _dlRectangle4Para);    
        }
       
        private bool MainFindBlob(HObject _hbImg, Blob _blob,string fileName,string strFile, HWindow _hwWinID,
            string strClamb,HTuple _htPos)
        {
            HTuple _htWidth, _htHeight;
            HTuple _htRow,_htColumn,_htArea;
            HObject _htReion;

            HOperatorSet.GenEmptyObj(out _htReion);
            DataTable dtCircle = new DataTable();

           
            double[] dlPatternCircle = new double[3];

            dtCircle = fc.ReadCSV(Application.StartupPath + fileName).Copy();

            double[] dlCircle = new double[dtCircle.Columns.Count];
            for (int i = 0; i < dtCircle.Columns.Count; i++)
            {
                dlCircle[i] = double.Parse(dtCircle.Columns[i].ToString());
            }           
            
            DataTable dtBlobPara = new DataTable();

            dtBlobPara = fc.ReadCSV(Application.StartupPath + strFile).Copy();

            double[] dlBlobPara = new double[dtBlobPara.Columns.Count];

            for (int i = 0; i < dtBlobPara.Columns.Count; i++)
            {
                dlBlobPara[i] = double.Parse(dtBlobPara.Columns[i].ToString());
            }
           
            try
            {
           
            HOperatorSet.GetImageSize(_hbImg, out _htWidth, out _htHeight);

            _htReion = _blob.GenThreshold(dlCircle[0], dlCircle[1], dlCircle[2], _hbImg, dlBlobPara[0], dlBlobPara[1]);

            if (_htReion == null)
            {
                
                m_displayPara.set_display_font(_hwWinID, 30, "mono", "true", "false");
                HOperatorSet.SetLineWidth(_hwWinID, 3);
                m_displayPara.SetLine("red", "margin", _hwWinID);
                HOperatorSet.SetTposition(_hwWinID, _htPos, 0);
                HOperatorSet.WriteString(_hwWinID,"面积=" + "0");
                HOperatorSet.DispRectangle1(_hwWinID, 0, 0, 960, 1280);
                HOperatorSet.DispCircle(_hwWinID, dlCircle[0], dlCircle[1], dlCircle[2]);
                HOperatorSet.SetTposition(_hwWinID, 860,1180);
                HOperatorSet.WriteString(_hwWinID, strClamb);
                return false;               
              
            }
            else
            {
                HOperatorSet.AreaCenter(_htReion, out _htRow, out _htColumn, out _htArea);
                if (_htArea.D > dlBlobPara[2])
                {
                    m_displayPara.set_display_font(_hwWinID, 30, "mono", "true", "false");
                    HOperatorSet.SetLineWidth(_hwWinID, 3);
                    m_displayPara.SetLine("green", "margin", _hwWinID);
                    HOperatorSet.SetTposition(_hwWinID, _htPos, 0);
                    HOperatorSet.WriteString(_hwWinID, "面积=" + _htArea[0].D.ToString());
                    HOperatorSet.SetTposition(_hwWinID, 860, 1180);
                    HOperatorSet.WriteString(_hwWinID, strClamb);
                    HOperatorSet.DispRectangle1(_hwWinID, 0, 0, 960, 1280);
                    HOperatorSet.DispCircle(_hwWinID, dlCircle[0], dlCircle[1], dlCircle[2]);
                    HOperatorSet.DispObj(_htReion, _hwWinID);
                    return true;
                }
                else
                {
                    m_displayPara.set_display_font(_hwWinID, 30, "mono", "true", "false");
                    HOperatorSet.SetLineWidth(_hwWinID, 3);
                    m_displayPara.SetLine("red", "margin", _hwWinID);
                    HOperatorSet.SetTposition(_hwWinID, _htPos, 0);
                    HOperatorSet.WriteString(_hwWinID, "面积=" + _htArea[0].D.ToString());
                    HOperatorSet.SetTposition(_hwWinID, 860, 1180);
                    HOperatorSet.WriteString(_hwWinID, strClamb);
                    HOperatorSet.DispRectangle1(_hwWinID, 0, 0, 960, 1280);
                    HOperatorSet.DispCircle(_hwWinID, dlCircle[0], dlCircle[1], dlCircle[2]);
                    HOperatorSet.DispObj(_htReion, _hwWinID);
                    return false;
                }
               
            }
           
            }
            catch (System.Exception ex)
            {
                m_displayPara.set_display_font(_hwWinID, 30, "mono", "true", "false");
                HOperatorSet.SetLineWidth(_hwWinID, 3);
                m_displayPara.SetLine("red", "margin", _hwWinID);
                HOperatorSet.SetTposition(_hwWinID, _htPos, 0);
                HOperatorSet.WriteString(_hwWinID, "面积=" + "0");
                HOperatorSet.DispRectangle1(_hwWinID, 0, 0, 860, 1180);
                HOperatorSet.DispCircle(_hwWinID, dlCircle[0], dlCircle[1], dlCircle[2]);
                HOperatorSet.SetTposition(_hwWinID, 1180, 860);
                HOperatorSet.WriteString(_hwWinID, strClamb);
                return false;
            }
            
        }
      

       

    
        public int m_iOldTrigger = 0;
        public int m_iTigger = 0;
     
     
        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
      
        }

      
        private double[] ReadCircleP(string _strFile)
        {
            DataTable _dtCircleP = new DataTable();

            _dtCircleP = fc.ReadCSV(Application.StartupPath + _strFile).Copy();

            double[] _dlCircleP = new double[_dtCircleP.Columns.Count];

            for (int i = 0; i < _dtCircleP.Columns.Count; i++)
            {
                _dlCircleP[i] = double.Parse(_dtCircleP.Columns[i].ToString());
            }
            return _dlCircleP;
        }
        private double[] ReadMeasurePara(string fileName)
        {
            string[] _strMeasurePara;
            _strMeasurePara = fc.ReadIni(fileName);
            double[] _dlMeasurePara = new double[_strMeasurePara.Length];
            for(int i=0;i<_strMeasurePara.Length;i++)
            {
                _dlMeasurePara[i] = double.Parse(_strMeasurePara[i]);
            }
            return _dlMeasurePara;
        }
        object obj = new object();
        private bool CameraAnaly(HObject _hbImg, Distance _distance, HWindow _hwWinID, 
            int _iCamera)
        {
            lock (obj)
            {
                HTuple _htWidth, _htHeight;
                string _strFileName;
                bool _bBlob = true;


                double[] _dlMeasureDist = new double[5];

                double[] _dlMeasurePara1 = new double[8];
                double[] _dlMeasurePara2 = new double[8];
                double[] _dlMeasurePara3 = new double[8];
                double[] _dlMeasurePara4 = new double[8];

                double[] _dlRectangle2Para11 = new double[5];
                double[] _dlRectangle2Para12 = new double[5];
                double[] _dlRectangle2Para13 = new double[5];
                double[] _dlRectangle2Para14 = new double[5];

                double[] _dlRectangle2Para21 = new double[5];
                double[] _dlRectangle2Para22 = new double[5];
                double[] _dlRectangle2Para23 = new double[5];
                double[] _dlRectangle2Para24 = new double[5];

                double[] _dlRectangle2Para31 = new double[5];
                double[] _dlRectangle2Para32 = new double[5];
                double[] _dlRectangle2Para33 = new double[5];
                double[] _dlRectangle2Para34 = new double[5];

                double[] _dlRectangle2Para41 = new double[5];
                double[] _dlRectangle2Para42 = new double[5];
                double[] _dlRectangle2Para43 = new double[5];
                double[] _dlRectangle2Para44 = new double[5];
                try
                {
                    _strFileName = Application.StartupPath + "\\MeasurePara1.ini";
                    _dlMeasurePara1 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\MeasurePara2.ini";
                    _dlMeasurePara2 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\MeasurePara3.ini";
                    _dlMeasurePara3 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\MeasurePara4.ini";
                    _dlMeasurePara4 = ReadMeasurePara(_strFileName);

                    _strFileName = Application.StartupPath + "\\Rectangle11.ini";
                    _dlRectangle2Para11 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle12.ini";
                    _dlRectangle2Para12 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle13.ini";
                    _dlRectangle2Para13 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle14.ini";
                    _dlRectangle2Para14 = ReadMeasurePara(_strFileName);

                    _strFileName = Application.StartupPath + "\\Rectangle21.ini";
                    _dlRectangle2Para21 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle22.ini";
                    _dlRectangle2Para22 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle23.ini";
                    _dlRectangle2Para23 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle24.ini";
                    _dlRectangle2Para24 = ReadMeasurePara(_strFileName);

                    _strFileName = Application.StartupPath + "\\Rectangle31.ini";
                    _dlRectangle2Para31 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle32.ini";
                    _dlRectangle2Para32 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle33.ini";
                    _dlRectangle2Para33 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle34.ini";
                    _dlRectangle2Para34 = ReadMeasurePara(_strFileName);

                    _strFileName = Application.StartupPath + "\\Rectangle41.ini";
                    _dlRectangle2Para41 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle42.ini";
                    _dlRectangle2Para42 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle43.ini";
                    _dlRectangle2Para43 = ReadMeasurePara(_strFileName);
                    _strFileName = Application.StartupPath + "\\Rectangle44.ini";
                    _dlRectangle2Para44 = ReadMeasurePara(_strFileName);
                    //Plc.ReadMessage();

                    string strClamb = "";
                    //string strClamb = Plc.ByteClamp.ToString();
                    HOperatorSet.GetImageSize(_hbImg, out _htWidth, out _htHeight);

                    _hwWinID.SetPart(0, 0, _htHeight, _htWidth);

                    HOperatorSet.DispObj(_hbImg, _hwWinID);
                    #region "计算程序"
                    switch (_iCamera)
                    {
                        case 1:

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para11, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara1[0] && _dlMeasureDist[4] < _dlMeasurePara1[1])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 0, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 0, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para12, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara1[2] && _dlMeasureDist[4] < _dlMeasurePara1[3])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 150, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 150, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para13, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara1[4] && _dlMeasureDist[4] < _dlMeasurePara1[5])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 300, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 300, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para14, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara1[6] && _dlMeasureDist[4] < _dlMeasurePara1[7])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 450, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 450, strClamb);
                                _bBlob = false;
                            }

                            break;


                        case 2:
                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para21, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara2[0] && _dlMeasureDist[4] < _dlMeasurePara2[1])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 0, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 0, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para22, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara2[2] && _dlMeasureDist[4] < _dlMeasurePara2[3])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 150, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 150, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para23, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara2[4] && _dlMeasureDist[4] < _dlMeasurePara2[5])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 300, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 300, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para24, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara2[6] && _dlMeasureDist[4] < _dlMeasurePara2[7])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 450, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 450, strClamb);
                                _bBlob = false;
                            }
                            break;


                        case 3:
                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para31, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara3[0] && _dlMeasureDist[4] < _dlMeasurePara3[1])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 0, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 0, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para32, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara3[2] && _dlMeasureDist[4] < _dlMeasurePara3[3])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 150, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 150, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para33, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara3[4] && _dlMeasureDist[4] < _dlMeasurePara3[5])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 300, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 300, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para34, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara3[6] && _dlMeasureDist[4] < _dlMeasurePara3[7])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 450, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 450, strClamb);
                                _bBlob = false;
                            }
                            break;


                        case 4:
                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para41, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara4[0] && _dlMeasureDist[4] < _dlMeasurePara4[1])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 0, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 0, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para42, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara4[2] && _dlMeasureDist[4] < _dlMeasurePara4[3])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 150, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 150, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para43, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara4[4] && _dlMeasureDist[4] < _dlMeasurePara4[5])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 300, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 300, strClamb);
                                _bBlob = false;
                            }

                            _dlMeasureDist = _distance.MeasureDistance(_dlRectangle2Para44, _hbImg);
                            if (_dlMeasureDist[4] > _dlMeasurePara4[6] && _dlMeasureDist[4] < _dlMeasurePara4[7])
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, true, 450, strClamb);
                            }
                            else
                            {
                                AnalyResult(_dlMeasureDist, _hwWinID, false, 450, strClamb);
                                _bBlob = false;
                            }
                            break;
                    }
                    DispForm(_hwWinID, _bBlob);
                    #endregion
                    return _bBlob;
                }
                catch
                { return false; }
            }
        }
       private void ThreadSub1()
        {
            HObject _hbWindowImage;
            MainForm fm = (MainForm)FormHandle;
             try
                {
                    camera1.AnalyEvent.WaitOne();
                    camera1.AnalyEvent.Reset();
                    string strDate = DateTime.Now.ToString();
                    string strBmp = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString()
                        + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                    m_strDate1 = strBmp;

                    //m_hbImg1 = camera1.Image;

                    HOperatorSet.CopyObj(camera1.Image, out m_hbImg1, 1, 1); 

                    camera1.Image.Dispose();

                    m_bAnaly1 = CameraAnaly(m_hbImg1, m_distance1, WinID1,1);
                  
                    //Plc.ReadMessage();
                                      
                 
                    if (!m_bAnaly1)
                    {
                        if(System.IO.Directory.Exists("D:\\" + "Camera1\\") == false)
                        {
                        
                        }
                        else
                        {
                            try
                            {
                                //HOperatorSet.DumpWindowImage(out _hbWindowImage, WinID1);
                                HOperatorSet.WriteImage(m_hbImg1, "bmp", 0, "D:\\" + "Camera1\\" + strBmp + ".bmp");
                                //HOperatorSet.WriteImage(m_hbImg1, "jpg", 0, "D:\\" + "Camera1\\" + strBmp + ".jpg");
                            }
                            catch
                            { }
                            
                        }
                      
                    }
                    m_bAnalyFinish1 = true;
                    m_hbImg1.Dispose();
                }
                catch (System.Exception ex)
                {
                    m_bAnalyFinish1 = true;
                }

             m_AnalyFinishEvent1.Set();
            
          }
       private void ThreadSub2()
       {
           HObject _hbWindowImage;
           MainForm fm = (MainForm)FormHandle;
          
               try
               {

                   camera2.AnalyEvent.WaitOne();
                   camera2.AnalyEvent.Reset();
                   string strDate = DateTime.Now.ToString();
                   string strBmp = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString()
                       + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                   m_strDate2 = strBmp;

                   //m_hbImg2 = camera2.Image;

                   HOperatorSet.CopyObj(camera2.Image, out m_hbImg2, 1, 1);        

                   camera2.Image.Dispose();
                   m_bAnaly2 = CameraAnaly(m_hbImg2, m_distance2, WinID2,2);
                   //Plc.ReadMessage();                  
                  
                   if (!m_bAnaly2)
                   {
                       if (System.IO.Directory.Exists("D:\\" + "Camera2\\") == false)
                       {

                       }
                       else
                       {
                           try
                           {
                               HOperatorSet.WriteImage(m_hbImg2, "bmp", 0, "D:\\" + "Camera2\\" + strBmp + ".bmp");
                               //HOperatorSet.DumpWindowImage(out _hbWindowImage, WinID2);
                               //HOperatorSet.WriteImage(_hbWindowImage, "bmp", 0, "D:\\" + "Camera2\\" + strBmp + ".bmp");
                               //HOperatorSet.WriteImage(m_hbImg2, "jpg", 0, "D:\\" + "Camera2\\" + strBmp + ".jpg");
                           }
                           catch
                           { }
                       }

                   }
                   
                   m_bAnalyFinish2 = true;
                   m_hbImg2.Dispose();
               }
               catch (System.Exception ex)
               {
                   m_bAnalyFinish2 = true;  

               }

               m_AnalyFinishEvent2.Set();
       }
       private void ThreadSub3()
       {
           HObject _hbWindowImage;
           MainForm fm = (MainForm)FormHandle;
           try
           {
               camera3.AnalyEvent.WaitOne();
               camera3.AnalyEvent.Reset();
               string strDate = DateTime.Now.ToString();
               string strBmp = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString()
                   + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
               m_strDate3 = strBmp;

               //m_hbImg3 = camera3.Image;
               HOperatorSet.CopyObj(camera3.Image, out m_hbImg3, 1, 1);        

               camera3.Image.Dispose();
               
               m_bAnaly3 = CameraAnaly(m_hbImg3, m_distance3, WinID3, 3);

               //Plc.ReadMessage();
               

               if (!m_bAnaly3)
               {
                   if (System.IO.Directory.Exists("D:\\" + "Camera3\\") == false)
                   {

                   }
                   else
                   {
                       try
                       {
                           HOperatorSet.WriteImage(m_hbImg3, "bmp", 0, "D:\\" + "Camera3\\" + strBmp + ".bmp");
                           //HOperatorSet.DumpWindowImage(out _hbWindowImage, WinID3);
                           //HOperatorSet.WriteImage(_hbWindowImage, "bmp", 0, "D:\\" + "Camera3\\" + strBmp + ".bmp");
                           //HOperatorSet.WriteImage(m_hbImg3, "jpg", 0, "D:\\" + "Camera3\\" + strBmp + ".jpg");
                       }
                       catch
                       { }
                   }

               }
               m_bAnalyFinish3 = true;
               m_hbImg3.Dispose();
           }
           catch (System.Exception ex)
           {
               m_bAnalyFinish3 = true;
           }

           m_AnalyFinishEvent3.Set();

       }
       private void ThreadSub4()
       {
           HObject _hbWindowImage;
           MainForm fm = (MainForm)FormHandle;

           try
           {

               camera4.AnalyEvent.WaitOne();
               camera4.AnalyEvent.Reset();
               string strDate = DateTime.Now.ToString();
               string strBmp = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString()
                   + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
               m_strDate4 = strBmp;

               HOperatorSet.CopyObj(camera4.Image,out m_hbImg4, 1, 1);              
               camera4.Image.Dispose();

               m_bAnaly4 = CameraAnaly(m_hbImg4, m_distance4, WinID4, 4);
               //Plc.ReadMessage();                  
          

               if (!m_bAnaly4)
               {
                   if (System.IO.Directory.Exists("D:\\" + "Camera4\\") == false)
                   {

                   }
                   else
                   {
                       try
                       {
                           HOperatorSet.WriteImage(m_hbImg4, "bmp", 0, "D:\\" + "Camera4\\" + strBmp + ".bmp");
                           //HOperatorSet.DumpWindowImage(out _hbWindowImage, WinID4);
                           //HOperatorSet.WriteImage(_hbWindowImage, "bmp", 0, "D:\\" + "Camera4\\" + strBmp + ".bmp");
                           //HOperatorSet.WriteImage(m_hbImg4, "jpg", 0, "D:\\" + "Camera4\\" + strBmp + ".jpg");
                       }
                       catch
                       { }
                   }

               }

               m_bAnalyFinish4 = true;
               m_hbImg4.Dispose();
           }
           catch (System.Exception ex)
           {
               m_bAnalyFinish4 = true;

           }

           m_AnalyFinishEvent4.Set();
       }

     

       private void btnStop_Click(object sender, EventArgs e)
       {
           btnStart.Enabled = true;
           btnStop.Enabled = false;
           if (m_AnalyThread1 != null)
           {
               if (m_AnalyThread1.IsAlive)
               {
                   m_bAnalyThread1 = false;
                   m_AnalyThread1.Abort();
               }

           }
           if (m_AnalyThread2 != null)
           {
               if (m_AnalyThread2.IsAlive)
               {
                   m_bAnalyThread2 = false;
                   m_AnalyThread2.Abort();
               }

           }
           if (m_AnalyThread3 != null)
           {
               if (m_AnalyThread3.IsAlive)
               {
                   m_bAnalyThread3 = false;
                   m_AnalyThread3.Abort();
               }

           }
           if (m_AnalyThread4 != null)
           {
               if (m_AnalyThread4.IsAlive)
               {
                   m_bAnalyThread4 = false;
                   m_AnalyThread4.Abort();
               }
           }

         
           if (m_PlcThread != null)
           {
               if (m_PlcThread.IsAlive)
               {
                   m_bPlcThread = false;
                   m_PlcThread.Abort();
               }

           }
       }

     

       private void tsmiFindCircle1_Click(object sender, EventArgs e)
       {
           m_bAnaly1 = CameraAnaly(m_hbImg1, m_distance1, WinID1,1);
       }

       private void tsmiFindCircle2_Click(object sender, EventArgs e)
       {
           m_bAnaly2 = CameraAnaly(m_hbImg2, m_distance2, WinID2,2);
       }
       private void tsmiFindCircle3_Click(object sender, EventArgs e)
       {
           m_bAnaly3 = CameraAnaly(m_hbImg3, m_distance3, WinID3, 3);
       }

       private void tsmiFindCircle4_Click(object sender, EventArgs e)
       {
           m_bAnaly4 = CameraAnaly(m_hbImg4, m_distance4, WinID4, 4);
       }

       private void btnExit_Click(object sender, EventArgs e)
       {
           this.Close();
       }
       int m_iDate = DateTime.Now.Day;
       private void timer1_Tick(object sender, EventArgs e)
       {
           string tempStr1;
           int tempInt1;
           tempStr1 = m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 230);
           if (tempStr1 == "" || tempStr1 == null)
               tempInt1 = 0;
           else
               tempInt1 = int.Parse(tempStr1);
           if (tempInt1== 1)  //(int.Parse(m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 230)) 
           {
               btnConnectState.BackColor = Color.Green;
               m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 230, 2);
           }
           else 
           {
               btnConnectState.BackColor = Color.Red;
               m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 230, 1);
           }


           string tempStr2;
           int tempInt2;
           tempStr2 = m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 200);
           if (tempStr2 == "" || tempStr2 == null)
               tempInt2 = 0;
           else
               tempInt2 = int.Parse(tempStr2);
           if (tempInt2 == 1) //(int.Parse(m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 200))
           {
               btnTrigger.BackColor = Color.Green;
             
              // m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 230, 2);
           }
           else //if ((int.Parse(m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 200)) == 0))
           {
               btnTrigger.BackColor = Color.Red;
             
               //m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 230, 1);
           }



           string tempStr3;
           int tempInt3;
           tempStr3 = m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 260);
           if (tempStr3 == "" || tempStr3 == null)
               tempInt3 = 0;
           else
               tempInt3 = int.Parse(tempStr3);
           if (tempInt3 == 1) //(int.Parse(m_plc.ReadFormPlc(m_iPcIp, m_iPlcIp, 260))
           {               
               tbPLCState.Text = "视觉启用中";
               tbPLCState.BackColor = Color.Green;
               //m_plc.WriteToPlc(m_iPcIp, m_iPlcIp, 230, 1);
           }
           else
           {
              
               tbPLCState.Text = "视觉被屏蔽";
               tbPLCState.BackColor = Color.Red;
           }

           tbNGPec.Text = m_uiNGPec.ToString();

           tbOKPec.Text = m_uiOKPec.ToString();

           tbTotalPec.Text = m_uiTotalPec.ToString();

           tbOKRate.Text = m_dlOKRate.ToString();

         

       }

       private void toolStripButton1_Click(object sender, EventArgs e)
       {
           m_bSnapOnce = true;
       }

       private void btnData_Click(object sender, EventArgs e)
       {

       }

       private void btnClear_Click(object sender, EventArgs e)
       {
           m_dlOKRate = 0;
           m_uiNGPec = 0;
           m_uiOKPec = 0;
           m_uiTotalPec = 0;
       }

       private void btnConnectState_Click(object sender, EventArgs e)
       {

       }

       private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
       {
           Help help = new Help();
           help.Show();
       }

      
     

   

      
      
    }
}
