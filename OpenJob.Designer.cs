﻿namespace C17Software
{
    partial class OpenJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.libFilesName = new System.Windows.Forms.ListBox();
            this.btnJobOpenCancel = new System.Windows.Forms.Button();
            this.btnJobOpenSure = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // libFilesName
            // 
            this.libFilesName.FormattingEnabled = true;
            this.libFilesName.ItemHeight = 12;
            this.libFilesName.Location = new System.Drawing.Point(12, 12);
            this.libFilesName.Name = "libFilesName";
            this.libFilesName.Size = new System.Drawing.Size(493, 292);
            this.libFilesName.TabIndex = 1;
            // 
            // btnJobOpenCancel
            // 
            this.btnJobOpenCancel.Location = new System.Drawing.Point(364, 333);
            this.btnJobOpenCancel.Name = "btnJobOpenCancel";
            this.btnJobOpenCancel.Size = new System.Drawing.Size(84, 39);
            this.btnJobOpenCancel.TabIndex = 4;
            this.btnJobOpenCancel.Text = "取消";
            this.btnJobOpenCancel.UseVisualStyleBackColor = true;
            this.btnJobOpenCancel.Click += new System.EventHandler(this.btnJobOpenCancel_Click);
            // 
            // btnJobOpenSure
            // 
            this.btnJobOpenSure.Location = new System.Drawing.Point(62, 333);
            this.btnJobOpenSure.Name = "btnJobOpenSure";
            this.btnJobOpenSure.Size = new System.Drawing.Size(84, 39);
            this.btnJobOpenSure.TabIndex = 3;
            this.btnJobOpenSure.Text = "确定";
            this.btnJobOpenSure.UseVisualStyleBackColor = true;
            this.btnJobOpenSure.Click += new System.EventHandler(this.btnJobOpenSure_Click);
            // 
            // OpenJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 385);
            this.Controls.Add(this.btnJobOpenCancel);
            this.Controls.Add(this.btnJobOpenSure);
            this.Controls.Add(this.libFilesName);
            this.Name = "OpenJob";
            this.Text = "OpenJob";
            this.Load += new System.EventHandler(this.OpenJob_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox libFilesName;
        private System.Windows.Forms.Button btnJobOpenCancel;
        private System.Windows.Forms.Button btnJobOpenSure;
    }
}